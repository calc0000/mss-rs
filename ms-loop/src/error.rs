use core;
use packet;

use std::convert;
use std::io;

#[derive(Debug)]
pub enum Error {
    Binary(core::binary::Error),
    Io(io::Error),
    Packet(packet::Error),
    ListenFailed(Option<io::Error>),
}

impl convert::From<core::binary::Error> for Error {
    fn from (x: core::binary::Error) -> Error {
        Error::Binary(x)
    }
}
impl convert::From<packet::Error> for Error {
    fn from (x: packet::Error) -> Error {
        Error::Packet(x)
    }
}
impl convert::From<io::Error> for Error {
    fn from (x: io::Error) -> Error {
        Error::Io(x)
    }
}
