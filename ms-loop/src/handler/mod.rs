use std::collections::HashMap;
use std::io;
use std::net;
use std::sync::mpsc::Sender;
use mio;

use self::client::Client;
use loop_::{EventLoop, Token};
use {core, config, packet};

mod client;

pub type Listener = mio::tcp::TcpListener;
pub type Stream = mio::tcp::TcpStream;

#[derive(Clone, Debug)]
pub enum InputMessage {
    Disconnect(Token),
    DirtyDisconnect(Token),
    ConnectResponse(Token),
    Packet(Token, packet::server::Packet),
    Pong(Token),
    Shutdown,
}

#[derive(Clone, Debug)]
pub enum OutputMessage {
    Disconnect(Token),
    DirtyDisconnect(Token),
    ConnectRequest(Token, net::SocketAddr),
    Packet(Token, packet::client::Packet),
    Ping(Token),
}

#[derive(Clone, Debug)]
pub enum TimeoutOperation {
    PingSent(Token),
    PingTimeout(Token),
}

#[derive(Debug)]
enum Action {
    None,
    TryFlush,
}

#[derive(Debug)]
enum Error {
    AcceptFailed,

    // these errors do not constitute shutting down of the loop, but do cause a client dirty
    // disconnect
    Io(io::Error),
    ClientError,

    // these errors constitute a clean client disconnect
    ClientDisconnect,

    // these errors do cause the loop to shutdown
    DownstreamDisconnect,
}

pub struct Handler {
    clients:        HashMap<Token, (bool, Client)>,
    timeouts:       HashMap<Token, mio::Timeout>,
    listener:       Listener,
    listener_token: Token,
    token_index:    usize,
    downstream:     Sender<OutputMessage>,
}

impl Handler {
    pub fn new (
        listener:       Listener,
        listener_token: Token,
        token_index:    usize,
        downstream:     Sender<OutputMessage>,
        ) -> Handler {
            Handler {
                clients:        HashMap::new(),
                timeouts:       HashMap::new(),
                listener:       listener,
                listener_token: listener_token,
                token_index:    token_index,
                downstream:     downstream,
            }
        }

    pub fn next_token (&mut self) -> Token {
        let index = self.token_index;
        self.token_index += 1;
        mio::Token(index)
    }
}

impl Handler {
    fn try_flush (&mut self, eloop: &mut EventLoop, token: Token) -> Result<Action, Error> {
        if let Some(&mut (ref mut waiting_for_write, ref mut client)) = self.clients.get_mut(&token) {
            // try to flush the client
            match client.flush_write() {
                // the write failed
                Err(e) => {
                    error!("error flushing write for client at {:?}: {:?}", client.addr, e);
                    return Err(Error::ClientError);
                },

                // the write would've blocked
                Ok(client::OperationResult::WouldBlock) => {

                    // were we waiting for it to be writeable?
                    if ! *waiting_for_write {
                        // reregister it with the writeable interest
                        match eloop.reregister(
                            client.as_ref(),
                            token,
                            mio::Interest::readable() | mio::Interest::writable() | mio::Interest::hup() | mio::Interest::error(),
                            mio::PollOpt::level()
                            ) {
                                Err(e) => {
                                    error!("failed to reregister client at {:?} for writeable: {:?}", client.addr, e);
                                    return Err(Error::Io(e));
                                },
                                _ => {},
                            }
                        *waiting_for_write = true;
                    }

                    Ok(Action::None)
                },

                // the write didn't block, so make sure that it's not waiting for a write
                Ok(client::OperationResult::Success(size)) => {
                    trace!("wrote {:?} bytes for client at {:?}", size, client.addr);

                    if *waiting_for_write {
                        // reregister it without the writeable interest
                        match eloop.reregister(
                            client.as_ref(),
                            token,
                            mio::Interest::readable() | mio::Interest::hup() | mio::Interest::error(),
                            mio::PollOpt::level()
                            ) {
                                Err(e) => {
                                    error!("failed to reregister client at {:?} for not writeable: {:?}", client.addr, e);
                                    return Err(Error::Io(e));
                                },
                                _ => {},
                            }
                        *waiting_for_write = false;
                    }

                    Ok(Action::None)
                },
            }
        } else {
            warn!("received flush request for stale token {:?}", token);
            Ok(Action::None)
        }
    }

    fn proc_disconnect (&mut self, eloop: &mut EventLoop, token: Token, dirty: bool) -> Result<Action, Error> {
        if self.clients.contains_key(&token) {
            if let Some(&mut (_, ref mut client)) = self.clients.get_mut(&token) {
                match eloop.deregister(client.as_ref()) {
                    Err(e) => return Err(Error::Io(e)),
                    _ => {},
                }
            }

            self.clients.remove(&token);
            self.timeouts.remove(&token);

            // and finally, notify the downstream
            match if dirty {
                self.downstream.send(OutputMessage::DirtyDisconnect(token))
            } else {
                self.downstream.send(OutputMessage::Disconnect(token))
            } {
                Err(_) => return Err(Error::DownstreamDisconnect),
                Ok(_) => {},
            }
        }

        Ok(Action::None)
    }

    fn proc_connect_response (&mut self, token: Token) -> Result<Action, Error> {
        if let Some(&mut (_, ref mut client)) = self.clients.get_mut(&token) {
            let hello = packet::server::Connect {
                version:     core::Version(config::VERSION),
                sub_version: "1".to_string(),
                receive_iv:  client.crypto.decrypt_iv,
                send_iv:     client.crypto.encrypt_iv,
                locale:      core::Locale(8),
            };

            match client.queue_connect(hello) {
                Err(e) => {
                    error!("error queueing hello packet: {:?}", e);

                    Err(Error::ClientError)
                },
                Ok(()) => {
                    trace!("queued hello packet for {:?}", client.addr);
                    Ok(Action::TryFlush)
                },
            }
        } else {
            warn!("received connect response for stale token {:?}", token);
            Ok(Action::None)
        }
    }

    fn proc_packet (&mut self, token: Token, packet: packet::server::Packet) -> Result<Action, Error> {
        if let Some(&mut (_, ref mut client)) = self.clients.get_mut(&token) {
            match client.queue_write(packet) {
                Err(e) => {
                    error!("error queuing packet: {:?}", e);

                    Err(Error::ClientError)
                },
                Ok(_) => {
                    trace!("queued packet for {:?}", client.addr);
                    Ok(Action::TryFlush)
                },
            }
        } else {
            warn!("received packet request for stale token {:?}", token);
            Ok(Action::None)
        }
    }

    fn proc_pong(&mut self, eloop: &mut EventLoop, token: Token) -> Result<Action, Error> {
        if let Some(&timeout) = self.timeouts.get(&token) {
            eloop.clear_timeout(timeout);
            self.timeouts.remove(&token);
            // pong packet received; in 30 seconds from now, the client will send a Ping packet to the client
            self.timeouts.insert(token, eloop.timeout_ms(TimeoutOperation::PingSent(token), 30000).unwrap());
        } else {
            warn!("received pong for stale token {:?}", token);
        }
        Ok(Action::None)
    }

    fn deregister_clients (&mut self, eloop: &mut EventLoop) -> Vec<Token> {
        let mut disconnected_clients = Vec::new();

        for (&token, &mut (_, ref mut client)) in self.clients.iter_mut() {
            disconnected_clients.push(token);
            match eloop.deregister(client.as_ref()) {
                Err(_) => {},
                Ok(_) => {},
            }
        }

        disconnected_clients
    }
    fn proc_shutdown (&mut self, eloop: &mut EventLoop) {
        let disconnected_clients = self.deregister_clients(eloop);
        self.clients.clear();
        self.timeouts.clear();

        for token in disconnected_clients {
            match self.downstream.send(OutputMessage::Disconnect(token)) {
                Err(_) => {},
                Ok(_) => {},
            }
        }

        eloop.shutdown();
    }
}

impl Handler {
    fn accept (&mut self, eloop: &mut EventLoop) -> Result<(), Error> {
        match self.listener.accept() {
            Err(e) => {
                error!("failed to accept incoming connection: {:?}", e);
                Err(Error::AcceptFailed)
            },
            Ok(None) => Ok(()),
            Ok(Some(stream)) => {
                let addr = match stream.peer_addr() {
                    Err(e) => {
                        error!("failed to get peer addr: {:?}", e);
                        return Err(Error::AcceptFailed);
                    },
                    Ok(x) => x,
                };

                let client = Client::new(addr.clone(), stream);
                let token = self.next_token();

                info!("acepted client from {:?}", addr);

                // register with the event loop
                match eloop.register_opt(
                    client.as_ref(),
                    token,
                    mio::Interest::readable() | mio::Interest::hup() | mio::Interest::error(),
                    mio::PollOpt::level()
                    ) {
                        Err(e) => {
                            error!("failed to register client at {:?}: {:?}", addr, e);
                            return Err(Error::AcceptFailed);
                        },
                        _ => {},
                    }

                // stash it in the HashMap
                self.clients.insert(token, (false, client));
                
                // set the timeout & send ping immediately
                self.timeouts.insert(token, eloop.timeout_ms(TimeoutOperation::PingSent(token), 0).unwrap());

                // send a message to the downstream
                match self.downstream.send(OutputMessage::ConnectRequest(token, addr)) {
                    Err(_) => Err(Error::DownstreamDisconnect),
                    Ok(_)  => Ok(()),
                }
            },
        }
    }

    fn readable (&mut self, eloop: &mut EventLoop, token: Token, hint: mio::ReadHint) -> Result<Action, Error> {
        if token == self.listener_token {
            match self.accept(eloop) {
                Err(e) => Err(e),
                Ok(_) => Ok(Action::None),
            }
        } else {
            if let Some(&mut (_, ref mut client)) = self.clients.get_mut(&token) {
                if hint.contains(mio::ReadHint::hup()) {
                    // client hung up
                    info!("client at {:?} disconnected", client.addr);
                    return Err(Error::ClientDisconnect);
                }

                if hint.contains(mio::ReadHint::error()) {
                    // client read error
                    info!("error from client {:?}", client.addr);
                    // pong packet received; in 30 seconds from now, the client will send a Ping packet to the client
                    return Err(Error::ClientError);
                }

                // read something
                match client.flush_read() {
                    // client read failed
                    Err(e) => {
                        info!("error reading from client at {:?}: {:?}", client.addr, e);
                        return Err(Error::ClientError);
                    },

                    // would block's are ok
                    Ok(client::OperationResult::WouldBlock) => {},

                    // we read something!
                    Ok(client::OperationResult::Success(size)) => {
                        trace!("read {} bytes from {:?}", size, client.addr);
                    },
                }

                // try to read some packets
                match client.try_read_all() {
                    // error parsing packets
                    Err(e) => {
                        info!("error parsing packets from client at {:?}: {:?}", client.addr, e);
                        return Err(Error::ClientError);
                    },

                    // we got packets!
                    Ok(packets) => {
                        // kick the packets over to the downstream
                        for packet in packets {
                            match self.downstream.send(OutputMessage::Packet(token, packet)) {
                                Err(_) => {
                                    error!("downstream disconnected");
                                    return Err(Error::DownstreamDisconnect);
                                },
                                _ => {},
                            }
                        }
                    },
                }

                Ok(Action::None)
            } else {
                warn!("received readable event for stale token {:?}", token);

                Ok(Action::None)
            }
        }
    }

    fn writable (&mut self, eloop: &mut EventLoop, token: Token) -> Result<Action, Error> {
        if let Some(&mut (ref mut waiting_for_write, _)) = self.clients.get_mut(&token) {
            if !*waiting_for_write {
                error!("received writable event, but not waiting for write on token {:?}!", token);
                return Ok(Action::None);
            }
        } else {
            warn!("received writable event for stale token {:?}", token);

            return Ok(Action::None);
        }

        // for borrow checker reasons, we do this here
        self.try_flush(eloop, token)
    }
}

impl Handler {
    fn handle_result (&mut self, eloop: &mut EventLoop, token: Token, res: Result<Action, Error>) {
        match res {
            Err(Error::AcceptFailed) => {}, // do nothing here for now

            // client dirty disconnect
            Err(Error::Io(_)) | Err(Error::ClientError) => {
                info!("dirty disconnect client {:?}", token);
                mio::Handler::notify(self, eloop, InputMessage::DirtyDisconnect(token));
            },

            // client clean disconnect
            Err(Error::ClientDisconnect) => {
                info!("clean disconnect client {:?}", token);
                mio::Handler::notify(self, eloop, InputMessage::Disconnect(token));
            },

            // these errors cause a loop shutdown
            Err(Error::DownstreamDisconnect) => eloop.shutdown(),

            Ok(Action::None) => {}, // nothing to do here

            Ok(Action::TryFlush) => {
                let result = self.try_flush(eloop, token);  // this will never return a TryFlush
                self.handle_result(eloop, token, result);
            },
        }
    }
}

impl mio::Handler for Handler {
    type Timeout = TimeoutOperation;
    type Message = InputMessage;

    fn readable (&mut self, eloop: &mut EventLoop, token: Token, hint: mio::ReadHint) {
        let result = Handler::readable(self, eloop, token, hint);
        self.handle_result(eloop, token, result);
    }

    fn writable (&mut self, eloop: &mut EventLoop, token: Token) {
        let result = Handler::writable(self, eloop, token);
        self.handle_result(eloop, token, result);
    }

    fn notify (&mut self, eloop: &mut EventLoop, message: InputMessage) {
        let (token, result) = match message {
            InputMessage::Disconnect(token)      => (token, self.proc_disconnect(eloop, token, false)),
            InputMessage::DirtyDisconnect(token) => (token, self.proc_disconnect(eloop, token, true)),
            InputMessage::ConnectResponse(token) => (token, self.proc_connect_response(token)),
            InputMessage::Packet(token, packet)  => (token, self.proc_packet(token, packet)),
            InputMessage::Pong(token)            => (token, self.proc_pong(eloop, token)),

            InputMessage::Shutdown => {
                self.proc_shutdown(eloop);
                return;
            },
        };

        self.handle_result(eloop, token, result);
    }

    fn timeout(&mut self, eloop: &mut EventLoop, timeout: TimeoutOperation) {
        match timeout {
            TimeoutOperation::PingSent(token)    => { // ping packet sent to client
                if let Some(&timeout) = self.timeouts.get(&token) {
                    eloop.clear_timeout(timeout);
                    self.timeouts.remove(&token);
                    // give the client 30 seconds to send a Pong packet, or PingTimeout will be invoked
                    self.timeouts.insert(token, eloop.timeout_ms(TimeoutOperation::PingTimeout(token), 30000).unwrap());
                    match self.downstream.send(OutputMessage::Ping(token)) {
                        Err(e) => warn!("failed to send ping to {:?}; {:?}", token, e),
                        _ => {}
                    };
                } else {
                    warn!("timeout invoked on stale token {:?}", token);
                }
            },
            TimeoutOperation::PingTimeout(token) => { // client did not send pong fast enough
                let result = self.proc_disconnect(eloop, token, false);
                self.handle_result(eloop, token, result);
            },
        };
    }
}
