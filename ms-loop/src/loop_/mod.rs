use std::net::ToSocketAddrs;
use std::sync::mpsc::Sender;
use mio::{self, tcp};

use Error;
use handler::{self, Handler};

pub type Control   = mio::Sender<handler::InputMessage>;
pub type EventLoop = mio::EventLoop<Handler>;
pub type Token     = mio::Token;

pub struct Loop {
    eloop:   EventLoop,
    handler: Handler,
}

impl Loop {
    pub fn new<A: ToSocketAddrs> (addrs: A, downstream: Sender<handler::OutputMessage>) -> Result<Loop, Error> {
        let socket = try!(tcp::TcpSocket::v4());

        try!(socket.set_reuseaddr(true));

        let mut error = None;

        for addr in try!(addrs.to_socket_addrs()) {
            match socket.bind(&addr) {
                Err(x) => error = Some(x),
                Ok(_) => break,
            }
        }

        if let Some(error) = error {
            Err(Error::ListenFailed(Some(error)))
        } else {
            let listener = try!(socket.listen(300));  // should the backlog be configurable?
            let listener_token = mio::Token(0);

            let mut eloop = try!(EventLoop::new());
            try!(eloop.register(&listener, listener_token));

            let handler = Handler::new(listener, listener_token, 1, downstream);

            Ok(Loop {
                eloop: eloop,
                handler: handler,
            })
        }
    }

    pub fn channel (&self) -> mio::Sender<handler::InputMessage> {
        self.eloop.channel()
    }

    pub fn run (&mut self) -> Result<(), Error> {
        Ok(try!(self.eloop.run(&mut self.handler)))
    }
}
