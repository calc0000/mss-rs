use std::net;

use super::user::User;

#[derive(PartialEq, Debug)]
pub enum LoginState {
    Disconnect, // TODO using this as a placeholder until i figure out a better idea for how to handle situations where the client should disconnect, as seen in proc_confirm_eula
    CheckPassword,
    CheckPin,
    CreatePin,
    UpdatePin,
    ConfirmGender,
    ConfirmEULA,
    WCSelect,
}

#[derive(Debug)]
pub struct Client {
    pub addr: net::SocketAddr,
    pub user: Option<User>,
    pub state: LoginState,
    pub login_fail_count: u8,
}
