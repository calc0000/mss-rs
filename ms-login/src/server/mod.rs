use prelude::*;

use std::{net, result, thread};
use std::collections::HashMap;

mod user;
mod client;

#[derive(Debug)]
pub enum Error {
    LoopShutdown,
}

pub type Result<T> = result::Result<T, Error>;

pub struct Server {
    eloop:    Option<msloop::Loop>,
    control:  msloop::Control,
    receiver: msloop::Receiver<msloop::OutputMessage>,
    clients:  HashMap<msloop::Token, client::Client>,
    dbcon:    postgres::Connection,
}

impl Server {
    pub fn get_client(&mut self, token: &msloop::Token) -> &mut client::Client {
        self.clients.get_mut(&token).unwrap()
    }

    fn proc_connect_request (&mut self, token: msloop::Token, addr: net::SocketAddr) -> Result<()> {
        debug!("connect request from {:?}: {:?}", token, addr);

        self.clients.insert(token, 
                            client::Client { 
                                addr: addr,
                                user: None,
                                state: client::LoginState::CheckPassword,
                                login_fail_count: 0,
                            });

        self.control.send(msloop::InputMessage::ConnectResponse(token)).map_err(|_| Error::LoopShutdown)
    }

    fn proc_authentication (&mut self, token: Token, packet: mspacket::client::Authentication) -> Result<()> {
        if let Some(client) = self.clients.get_mut(&token) {
            assert_eq!(client.state, client::LoginState::CheckPassword); // TODO hack log incorrect login state
            assert!(client.login_fail_count < 5); // TODO hack log high login fail count
            assert!(packet.username.len() >= 4 && packet.username.len() <= 13); // TODO hack log invalid username character length
            assert!(packet.password.len() >= 5 && packet.password.len() <= 12); // TODO hack log invalid password character length

            let mut res = mspacket::server::Authentication {
                login_result: mspacket::server::authentication::LoginResult::Success,
                reg_stat_id: 0,
                use_day: 0,
                account_id: mscore::AccountId(0),
                gender: mscore::Gender::Male,
                country_id: 0,
                grade_code: 0,
                username: String::new(),
                purchase_exp: 0,
                chat_block_reason: 0,
                chat_unblock_date: mscore::DateTime::zero_filetime(),
                register_date: mscore::DateTime::zero_filetime(),
                block_reason: 0,
                unblock_date: mscore::DateTime::zero_filetime(),
            };
        
            if let Some(mut user) = user::Provider::get_by_user(&mut self.dbcon, &packet.username).unwrap() {
                if user.unblock_date.gt(&chrono::UTC::now()) {
                    res.login_result = mspacket::server::authentication::LoginResult::Blocked;
                    res.block_reason = user.block_reason as u8;
                    res.unblock_date = user.unblock_date;
                } else if user.pass_hash != packet.password { // TODO hash
                    res.login_result = mspacket::server::authentication::LoginResult::IncorrectPassword;
                } else if !user.accepted_eula {
                    client.state = client::LoginState::ConfirmEULA;
                    res.login_result = mspacket::server::authentication::LoginResult::NotagreedEULA;
                    client.user = Some(user); // needed to set the eula status
                } else {
                    client.state = if user.gender == mscore::Gender::Undefined {
                                       client::LoginState::ConfirmGender
                                   } else {
                                       client::LoginState::CheckPin
                                   };
                    res.login_result = mspacket::server::authentication::LoginResult::Success;
                    res.account_id = user.id;
                    res.gender = user.gender;
                    res.username = user.username.clone();
                    user.login_time = mscore::DateTime(chrono::UTC::now());
                    client.user = Some(user);
                }
            } else {
                res.login_result = mspacket::server::authentication::LoginResult::NotRegistered;
            }
        
            if res.login_result != mspacket::server::authentication::LoginResult::NotagreedEULA &&
               res.login_result != mspacket::server::authentication::LoginResult::Success {
                client.login_fail_count += 1;
            }
            let response = msloop::InputMessage::Packet(token, mspacket::server::Packet::Authentication(res));

            self.control.send(response).map_err(|_| Error::LoopShutdown)
        } else { panic!() }
    }

    fn proc_set_gender (&mut self, token: Token, packet: mspacket::client::SetGender) -> Result<()> {
        if let Some(client) = self.clients.get_mut(&token) {
            assert_eq!(client.state, client::LoginState::ConfirmGender); // TODO hack log incorrect login state
            if packet.gender != mscore::Gender::Undefined {
                if let Some(user) = client.user.as_mut() {
                    assert_eq!(user.gender, mscore::Gender::Undefined); // TODO hack log this packet can only be sent whe user gender is Undefined
                    client.state = client::LoginState::CheckPin;
                    user.gender = packet.gender;
                    let response = mspacket::server::Packet::SetAccount(mspacket::server::SetAccount {
                        gender: packet.gender,
                        successful: user::Provider::save(&mut self.dbcon, &user).is_ok()
                    });
                    self.control.send(msloop::InputMessage::Packet(token, response)).map_err(|_| Error::LoopShutdown)
                } else { panic!() }
            } else { // user pressed cancel
                client.state = client::LoginState::CheckPassword;
                Ok(())
            }
        } else { panic!() }
    }

    fn proc_check_user_limit(&mut self, token: Token, packet: mspacket::client::CheckUserLimit) -> Result<()> {
        println!("user check limit on world {:?}", packet);
        let response = mspacket::server::Packet::SelectWorld(mspacket::server::SelectWorld {
            result: 0,
            characters: vec![],
            slot_count: 1,
        });
        self.control.send(msloop::InputMessage::Packet(token, response)).map_err(|_| Error::LoopShutdown)
    }

    fn proc_confirm_eula (&mut self, token: Token, packet: mspacket::client::ConfirmEula) -> Result<()> {
        if let Some(client) = self.clients.get_mut(&token) {
            assert_eq!(client.state, client::LoginState::ConfirmEULA); // TODO hack log incorrect login state
            if let Some(user) = client.user.as_mut() {
                assert!(!user.accepted_eula); // TODO hack log user should only send this response if they haven't accepted the EULA yet
                if packet.confirmed {
                    user.accepted_eula = true;
                    client.state = client::LoginState::CheckPassword; // client sends the checkpassword packet again
                    let response = mspacket::server::Packet::ConfirmEula(mspacket::server::ConfirmEula {
                        successful: user::Provider::save(&mut self.dbcon, &user).is_ok() // saving to db because the user is loaded from the db in checkpassword
                    });
                    self.control.send(msloop::InputMessage::Packet(token, response)).map_err(|_| Error::LoopShutdown)
                } else { 
                    // it should be noted that the client has strange behaviour. if the user presses cancel in the eula dlg, the client just closes (Disconnects on its' own).
                    // the response where successful is false indicates a database failure to save to the database, NOT reflecting whether or not the user pressed Ok or Cancel.
                    client.state = client::LoginState::Disconnect; // TODO hack log client should disconnect, maybe log if the user doesn't disconnect/continues to send packets?
                    Ok(())
                }
            } else { panic!() }
        } else { panic!() }
    }

    fn proc_check_pin (&mut self, token: Token, packet: mspacket::client::CheckPin) -> Result<()> {
        /*
           User pressing Cancel button sends CheckPin packet with ok=false;
           Success destroys the Pin dlg and sends packet requesting world list
           Incorrect creates the Please Enter your Security Pin dlg with Error enter a valid Security Pin appended.
           NotAssigned creates the Please register a 4 digit Security Pin dlg
               Pressing cancel creates a confirmination dlg You will not be able to play without assigning a Security Pin
                   Pressing cancel destroys both dlgs, and sends CheckPin with ok=false.
           Assigned creates the Please Enter your Security Pin dlg
           DBFail does not reset login state/call CUITitle::EnableLoginCtrl(true) (log in button is not pressed/unclickable); Must send another appropriate respone packet.
        */
        
        if let Some(client) = self.clients.get_mut(&token) {
            assert_eq!(client.state, client::LoginState::CheckPin); // TODO hack log incorrect login state
            assert!(client.login_fail_count < 5); // TODO hack log high login fail count
            if packet.pin_action != mspacket::client::check_pin::PinAction::Cancel {
                if let Some(user) = client.user.as_mut() {
                    assert_eq!(packet.account_id, user.id); // TODO hack log mismatched account_id with id stored in session from CheckPassword
                    // when is this packet sent without the user being Some in client...?
                    // TODO figure out a situation where this packet would be sent while user is None. until then, looks like PinResult::DBFail is going unused.
                    // let user = user::Provider::get_by_id(&mut self.dbcon, packet.account_id.0 as i64).unwrap(); // TODO store user in client object
                    let response = mspacket::server::Packet::CheckPin(mspacket::server::CheckPin {
                        result:
                            //if user.is_none() {
                            //    mspacket::server::check_pin::PinResult::DBFail
                            //} else {
                                //let user = user.unwrap();
                                if user.is_online {
                                    client.state = client::LoginState::CheckPassword;
                                    mspacket::server::check_pin::PinResult::AlreadyConnected // dlg "this user is already logged in"
                                } else if packet.init { // client auotmaticly creates pin dlg & sends this packet on check password success.
                                    if user.pin == String::new() {
                                        client.state = client::LoginState::CreatePin;
                                        mspacket::server::check_pin::PinResult::NotAssigned
                                    } else {
                                        // don't need to set state, should still be CheckPin
                                        mspacket::server::check_pin::PinResult::Assigned
                                    }
                                } else { // user is submitting a pin
                                    assert!(packet.pin.parse::<u32>().is_ok()); // TODO hack log could not parse to u32. client does not allow sending if it cannot parse. don't check on init as string is empty.
                                    if packet.pin == user.pin {
                                        if packet.pin_action == mspacket::client::check_pin::PinAction::Update { // Change Pin button
                                            client.state = client::LoginState::UpdatePin;
                                            mspacket::server::check_pin::PinResult::NotAssigned
                                        } else { // Log In button
                                            client.state = client::LoginState::WCSelect;
                                            mspacket::server::check_pin::PinResult::Success
                                        }
                                    } else {
                                        client.login_fail_count += 1; // TODO not sure if i should separate checkpassword & checkpin fail counts.
                                        mspacket::server::check_pin::PinResult::Incorrect
                                    }
                                }
                            //}
                    });
                    self.control.send(msloop::InputMessage::Packet(token, response)).map_err(|_| Error::LoopShutdown)
                } else { panic!() }
            } else {
                client.state = client::LoginState::CheckPassword;
                Ok(())
            }
        } else { panic!() }
    }

    fn proc_update_pin (&mut self, token: Token, packet: mspacket::client::UpdatePin) -> Result<()> {
        /*
           The client does not request worlds even if the update pin is
           successful. Regardless of whether or not the result is error,
           it resets its state back to login where it will enter the
           newly updated pin.

           Only send a response if packet.ok is true. This is false when
           the user clicks cancel in the Update Pin dialogue.

           update_pin is used not only for the update pin button, but
           for creating the pin as well.
        */
        if let Some(client) = self.clients.get_mut(&token) {
            assert!(client.state == client::LoginState::CreatePin || // proc_udpate_pin is used not only for the update pin button, but creating the pin as well
                    client.state == client::LoginState::UpdatePin);
            if packet.ok {
                if let Some(user) = client.user.as_mut() {
                    client.state = client::LoginState::CheckPassword;
                    user.pin = packet.pin;
                    let response = mspacket::server::Packet::UpdatePin(mspacket::server::UpdatePin {
                        error: user::Provider::save(&mut self.dbcon, &user).is_err()
                    });
                    self.control.send(msloop::InputMessage::Packet(token, response)).map_err(|_| Error::LoopShutdown)
                } else { panic!() }
            } else { // user pressed cancel
                client.state = client::LoginState::CheckPassword;
                Ok(())
            }
        } else { panic!() }
    }

    fn proc_world_info(&mut self, token: Token, _: mspacket::client::WorldInfo) -> Result<()> {
        // TODO unfinished
        let world = mspacket::server::WorldInfo {
            world_id: 0xFF,
            world_name: String::new(),
            world_state: 0,
            event_description: String::new(),
            event_exp_wse: 100,
            event_drop_wse: 100,
            block_char_creation: false,
            channel: vec![],
            balloon: vec![]
        };
        let response = mspacket::server::Packet::WorldInfo(mspacket::server::WorldInfo {
            world_id: 0,
            world_name: String::from("스카니아"),
            balloon: vec![mspacket::server::world_info::Balloon {
                position: (100, 100),
                message: String::from("안녕하세요:ㅈhello world")
            }],
            .. world.clone()
        });
        try!(self.control.send(msloop::InputMessage::Packet(token, response)).map_err(|_| Error::LoopShutdown));
        let response = mspacket::server::Packet::WorldInfo(world);
        self.control.send(msloop::InputMessage::Packet(token, response)).map_err(|_| Error::LoopShutdown)
    }

    fn proc_pong(&mut self, token: Token, _: mspacket::client::Pong) -> Result<()> {
        self.control.send(msloop::InputMessage::Pong(token)).map_err(|_| Error::LoopShutdown)
    }
}

impl Server {
    pub fn new<A: net::ToSocketAddrs> (addrs: A) -> result::Result<Server, msloop::Error> {
        let (sender, receiver) = msloop::channel();
        let eloop = try!(msloop::Loop::new(addrs, sender));
        let control = eloop.channel();
        Ok(Server {
            eloop:    Some(eloop),
            control:  control,
            receiver: receiver,
            clients:  HashMap::new(),
            dbcon:    postgres::Connection::connect("postgres://postgres@localhost/mss", &postgres::SslMode::None).unwrap(),
        })
    }

    pub fn run (mut self) -> result::Result<(), ()> {  //TODO: error type?
        // start the event loop
        let eloop = self.eloop.take();
        let guard = thread::scoped(move || {
            // this had better be a Some
            eloop.unwrap().run()
        });

        // start processing packets
        loop {
            let message = match self.receiver.recv() {
                Err(_) => break,
                Ok(x) => x,
            };

            let res = match message {
                msloop::OutputMessage::ConnectRequest(token, addr) => 
                    self.proc_connect_request(token, addr),
                msloop::OutputMessage::Packet(token, packet) => {
                    match packet {
                        // TODO hack log if client.user is None for packets that have scenarios where user can be None (Authentication, ExceptionLog, etc)
                        mspacket::client::Packet::Authentication(packet) =>
                            self.proc_authentication(token, packet),
                        mspacket::client::Packet::CheckUserLimit(packet) =>
                            self.proc_check_user_limit(token, packet),
                        mspacket::client::Packet::ConfirmEula(packet) =>
                            self.proc_confirm_eula(token, packet),
                        mspacket::client::Packet::SetGender(packet) =>
                            self.proc_set_gender(token, packet),
                        mspacket::client::Packet::CheckPin(packet) =>
                            self.proc_check_pin(token, packet),
                        mspacket::client::Packet::UpdatePin(packet) =>
                            self.proc_update_pin(token, packet),
                        mspacket::client::Packet::WorldInfo(packet) =>
                            self.proc_world_info(token, packet),
                        mspacket::client::Packet::Pong(packet) => 
                            self.proc_pong(token, packet),
                        mspacket::client::Packet::ExceptionLog(packet) => {
                            //TODO
                            println!("unimplemented {:?}", packet);
                            Ok(())
                        },
                        mspacket::client::Packet::Security(packet) => {
                            //TODO
                            println!("unimplemented {:?}", packet);
                            Ok(())
                        },
                    }
                },
                msloop::OutputMessage::Disconnect(token) |
                msloop::OutputMessage::DirtyDisconnect(token) => {
                    println!("client disconnecting {:?}", message);
                    if let Some(user) = self.get_client(&token).user.clone() {
                        println!("\tsaving user {:?}", user::Provider::save(&mut self.dbcon, &user));
                    }
                    self.clients.remove(&token);
                    Ok(())
                },
                msloop::OutputMessage::Ping(token) => {
                    let response = mspacket::server::Packet::Ping(mspacket::server::Ping);
                    self.control.send(msloop::InputMessage::Packet(token, response)).map_err(|_| Error::LoopShutdown)
                },
            };

            if let Err(err) = res {
                match err {
                    Error::LoopShutdown => break,
                }
            }
        }

        guard.join().map_err(|_| ())
    }
}
