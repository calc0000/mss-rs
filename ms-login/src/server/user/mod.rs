use prelude::*;

use mscore::Gender;

mod provider;
pub use self::provider::Provider;

#[derive(Clone, Debug)]
pub struct User {
    pub id: mscore::AccountId,
    pub username: String,
    pub pass_hash: String,
    pub pin: String,
    pub gender: Gender,
    pub block_reason: i16,
    pub unblock_date: mscore::DateTime,
    pub is_gm: bool,
    pub is_online: bool,
    pub creation_time: mscore::DateTime,
    pub login_time: mscore::DateTime,
    pub accepted_eula: bool,
}
