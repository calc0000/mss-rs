use prelude::*;
use super::User;

pub trait Provider {
    fn get_by_user (&mut self, username: &str) -> Result<Option<User>, Box<Error>>;
    fn get_by_id   (&mut self, id: i64) -> Result<Option<User>, Box<Error>>;
    fn save        (&mut self, user: &User) -> Result<u64, Box<Error>>;
}

impl<T: postgres::GenericConnection> Provider for T {
    fn get_by_user (&mut self, username: &str) -> Result<Option<User>, Box<Error>> {
        let statement = try!(self.prepare(
        "SELECT id, username, pass_hash, pin,
                gender, block_reason, unblock_date,
                is_gm, is_online, creation_time, login_time,
                accepted_eula
         FROM users
         WHERE username=$1"
         ));
        let result = try!(statement.query(&[&username]));
        let rows = result.iter().collect::<Vec<_>>();
        if rows.len() < 1 {
            Ok(None)
        } else {
            Ok(Some(User {
                id: mscore::AccountId(rows[0].get::<usize, i64>(0) as u32),
                username: rows[0].get(1),
                pass_hash: rows[0].get(2),
                pin: rows[0].get(3),
                gender: rows[0].get(4),
                block_reason: rows[0].get(5),
                unblock_date: rows[0].get(6),
                is_gm: rows[0].get(7),
                is_online: rows[0].get(8),
                creation_time: rows[0].get(9),
                login_time: rows[0].get(10),
                accepted_eula: rows[0].get(11),
            }))
        }
    }
    fn get_by_id (&mut self, id: i64) -> Result<Option<User>, Box<Error>> {
        let statement = try!(self.prepare(
        "SELECT id, username, pass_hash, pin,
                gender, block_reason, unblock_date,
                is_gm, is_online, creation_time, login_time,
                accepted_eula
         FROM users
         WHERE id=$1"
         ));
        let result = try!(statement.query(&[&id]));
        let rows = result.iter().collect::<Vec<_>>();
        if rows.len() < 1 {
            Ok(None)
        } else {
            Ok(Some(User {
                id: mscore::AccountId(rows[0].get::<usize, i64>(0) as u32),
                username: rows[0].get(1),
                pass_hash: rows[0].get(2),
                pin: rows[0].get(3),
                gender: rows[0].get(4),
                block_reason: rows[0].get(5),
                unblock_date: rows[0].get(6),
                is_gm: rows[0].get(7),
                is_online: rows[0].get(8),
                creation_time: rows[0].get(9),
                login_time: rows[0].get(10),
                accepted_eula: rows[0].get(11),
            }))
        }
    }
    fn save (&mut self, user: &User) -> Result<u64, Box<Error>> {
        let statement = try!(self.prepare(
            "UPDATE users
             SET    username=$2, pass_hash=$3, pin=$4, gender=$5, block_reason=$6, 
                    unblock_date=$7, is_gm=$8, is_online=$9, login_time=$10,
                    accepted_eula=$11
             WHERE  id=$1"
             ));
        Ok(try!(statement.execute(&[&(user.id.0 as i64), &user.username, &user.pass_hash, &user.pin,
                                          &user.gender, &user.block_reason, &user.unblock_date,
                                          &user.is_gm, &user.is_online, &user.login_time,
                                          &user.accepted_eula])))
    }
}
