use prelude::*;

#[derive(Clone, Debug)]
pub struct Connect {
    pub version:     mscore::Version,
    pub sub_version: String,
    pub receive_iv:  mscrypto::Iv,
    pub send_iv:     mscrypto::Iv,
    pub locale:      mscore::Locale,
}

impl binary::Writeable for Connect {
    fn write (&self, mut w: &mut Write) -> binary::Result<()> {
        try!(binary::write(w, &self.version));
        try!(binary::write(w, &self.sub_version));
        try!(binary::write(w, &self.receive_iv));
        try!(binary::write(w, &self.send_iv));
        try!(binary::write(w, &self.locale));
        Ok(())
    }
}
impl binary::Readable for Connect {
    fn read (mut r: &mut Read) -> binary::Result<Connect> {
        Ok(Connect {
            version:     try!(binary::Readable::read(r)),
            sub_version: try!(binary::Readable::read(r)),
            receive_iv:  try!(binary::Readable::read(r)),
            send_iv:     try!(binary::Readable::read(r)),
            locale:      try!(binary::Readable::read(r)),
        })
    }
}
