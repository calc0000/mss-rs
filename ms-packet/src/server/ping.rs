use prelude::*;

#[derive(Clone, Debug)]
pub struct Ping;

impl binary::Readable for Ping {
    fn read(_: &mut Read) -> binary::Result<Ping> {
        Ok(Ping)
    }
}
impl binary::Writeable for Ping {
    fn write(&self, _: &mut Write) -> binary::Result<()> {
        Ok(())
    }
}
