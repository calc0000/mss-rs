use prelude::*;

#[derive(Clone, Debug)]
pub struct Balloon {
    pub position: (u16, u16),
    pub message: String,
}

impl binary::Readable for Balloon {
    fn read(r: &mut Read) -> binary::Result<Balloon> {
        Ok(Balloon {
            position: (try!(binary::read(r)), try!(binary::read(r))),
            message: try!(binary::read(r))
        })
    }
}
impl binary::Writeable for Balloon {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        try!(self.position.0.write(w));
        try!(self.position.1.write(w));
        self.message.write(w)
    }
}

#[derive(Clone, Debug)]
pub struct Channel {
    channel_name: String,
    concurrent: u32,
    world_id: u8,
    channel_id: u8,
    adult_channel: bool
}

impl binary::Readable for Channel {
    fn read(r: &mut Read) -> binary::Result<Channel> {
        Ok(Channel {
            channel_name: try!(binary::read(r)),
            concurrent: try!(binary::read(r)),
            world_id: try!(binary::read(r)),
            channel_id: try!(binary::read(r)),
            adult_channel: try!(binary::read(r))
        })
    }
}
impl binary::Writeable for Channel {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        try!(self.channel_name.write(w));
        try!(self.concurrent.write(w));
        try!(self.world_id.write(w));
        try!(self.channel_id.write(w));
        self.adult_channel.write(w)
    }
}

#[derive(Clone, Debug)]
pub struct WorldInfo {
    pub world_id: u8,
    pub world_name: String,
    pub world_state: u8,
    pub event_description: String,
    pub event_exp_wse: u16,
    pub event_drop_wse: u16,
    pub block_char_creation: bool,
    pub channel: Vec<Channel>,
    pub balloon: Vec<Balloon>,
}

impl binary::Readable for WorldInfo {
    fn read(r: &mut Read) -> binary::Result<WorldInfo> {
        let mut ret = WorldInfo {
            world_id: try!(binary::read(r)),
            world_name: String::new(),
            world_state: 0,
            event_description: String::new(),
            event_exp_wse: 0,
            event_drop_wse: 0,
            block_char_creation: false,
            channel: vec![],
            balloon: vec![],
        };
        if ret.world_id < 0xFF { // world_id = 0xFF dictates end of WorldInfo
            ret.world_name = try!(binary::read(r));
            ret.world_state = try!(binary::read(r));
            ret.event_description = try!(binary::read(r));
            ret.event_exp_wse = try!(binary::read(r));
            ret.event_drop_wse = try!(binary::read(r));
            ret.block_char_creation = try!(binary::read(r));
            ret.channel = (0..try!(binary::read::<u8>(r))).flat_map(|_| binary::read(r)).collect();
            ret.balloon = (0..try!(binary::read::<u16>(r))).flat_map(|_| binary::read(r)).collect();
        }
        Ok(ret)
    }
}
impl binary::Writeable for WorldInfo {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        try!(self.world_id.write(w));
        if self.world_id < 0xFF { // world_id = 0xFF dictates end of WorldInfo
            try!(self.world_name.write(w));
            try!(self.world_state.write(w));
            try!(self.event_description.write(w));
            try!(self.event_exp_wse.write(w));
            try!(self.event_drop_wse.write(w));
            try!(self.block_char_creation.write(w));
            try!((self.channel.len() as u8).write(w));
            self.channel.iter().map(|ref channel| channel.write(w)).count();
            try!((self.balloon.len() as u16).write(w));
            self.balloon.iter().map(|ref balloon| balloon.write(w)).count();
        }
        Ok(())
    }
}
