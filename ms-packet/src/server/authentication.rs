use prelude::*;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum LoginResult {
    Success = 0x0,
    TempBlocked = 0x1,
    Blocked = 0x2, 
    Abandoned = 0x3, 
    IncorrectPassword = 0x4, 
    NotRegistered = 0x5, 
    DBFail = 0x6, 
    AlreadyConnected = 0x7, 
    NotConnectableWorld = 0x8, 
    Unknown = 0x9, 
    Timeout = 0xA, 
    NotAdult = 0xB, 
    AuthFail = 0xC, 
    ImpossibleIP = 0xD, 
    NotAuthorizedNexonID = 0xE, 
    NoNexonID = 0xF, 
    NotAuthorized = 0x10,
    InvalidRegionInfo = 0x11,
    InvalidBirthDate = 0x12,
    PassportSuspended = 0x13,
    IncorrectSSN2 = 0x14,
    WebAuthNeeded = 0x15,
    DeleteCharacterFailedOnGuildMaster = 0x16,
    NotagreedEULA = 0x17,
    DeleteCharacterFailedEngaged = 0x18,
}
impl LoginResult {
    pub fn from_u8(login_result: u8) -> Option<LoginResult> {
        match login_result {
            0x0 => Some(LoginResult::Success),
            0x1 => Some(LoginResult::TempBlocked),
            0x2 => Some(LoginResult::Blocked),
            0x3 => Some(LoginResult::Abandoned),
            0x4 => Some(LoginResult::IncorrectPassword),
            0x5 => Some(LoginResult::NotRegistered),
            0x6 => Some(LoginResult::DBFail),
            0x7 => Some(LoginResult::AlreadyConnected),
            0x8 => Some(LoginResult::NotConnectableWorld),
            0x9 => Some(LoginResult::Unknown),
            0xA => Some(LoginResult::Timeout),
            0xB => Some(LoginResult::NotAdult),
            0xC => Some(LoginResult::AuthFail),
            0xD => Some(LoginResult::ImpossibleIP),
            0xE => Some(LoginResult::NotAuthorizedNexonID),
            0xF => Some(LoginResult::NoNexonID),
            0x10 => Some(LoginResult::NotAuthorized),
            0x11 => Some(LoginResult::InvalidRegionInfo),
            0x12 => Some(LoginResult::InvalidBirthDate),
            0x13 => Some(LoginResult::PassportSuspended),
            0x14 => Some(LoginResult::IncorrectSSN2),
            0x15 => Some(LoginResult::WebAuthNeeded),
            0x16 => Some(LoginResult::DeleteCharacterFailedOnGuildMaster),
            0x17 => Some(LoginResult::NotagreedEULA),
            0x18 => Some(LoginResult::DeleteCharacterFailedEngaged),
            _ => None
        }
    }
}
impl binary::Readable for LoginResult {
    fn read(mut r: &mut Read) -> binary::Result<LoginResult> {
        Ok(LoginResult::from_u8(try!(binary::read(r))).unwrap())
    }
}
impl binary::Writeable for LoginResult {
    fn write(&self, mut w: &mut Write) -> binary::Result<()> {
        let login_result: u8 = match *self {
            LoginResult::Success => 0x0,
            LoginResult::TempBlocked => 0x1,
            LoginResult::Blocked => 0x2,
            LoginResult::Abandoned => 0x3,
            LoginResult::IncorrectPassword => 0x4,
            LoginResult::NotRegistered => 0x5,
            LoginResult::DBFail => 0x6,
            LoginResult::AlreadyConnected => 0x7,
            LoginResult::NotConnectableWorld => 0x8,
            LoginResult::Unknown => 0x9,
            LoginResult::Timeout => 0xA,
            LoginResult::NotAdult => 0xB,
            LoginResult::AuthFail => 0xC,
            LoginResult::ImpossibleIP => 0xD,
            LoginResult::NotAuthorizedNexonID => 0xE,
            LoginResult::NoNexonID => 0xF,
            LoginResult::NotAuthorized => 0x10,
            LoginResult::InvalidRegionInfo => 0x11,
            LoginResult::InvalidBirthDate => 0x12,
            LoginResult::PassportSuspended => 0x13,
            LoginResult::IncorrectSSN2 => 0x14,
            LoginResult::WebAuthNeeded => 0x15,
            LoginResult::DeleteCharacterFailedOnGuildMaster => 0x16,
            LoginResult::NotagreedEULA => 0x17,
            LoginResult::DeleteCharacterFailedEngaged => 0x18,
        };
        binary::write(w, &login_result)
    }
}

#[derive(Clone, Debug)]
pub struct Authentication {
    pub login_result:      LoginResult,
    pub reg_stat_id:       u8,
    pub use_day:           u32,
    pub account_id:        mscore::AccountId,
    pub gender:            mscore::Gender,
    pub country_id:        u8,
    pub grade_code:        u8,
    pub username:          String, // username, or email if you're korean
    pub purchase_exp:      u8,
    pub chat_block_reason: u8,
    pub chat_unblock_date: mscore::DateTime,
    pub register_date:     mscore::DateTime,
    pub block_reason:      u8,
    pub unblock_date:      mscore::DateTime,
}
impl binary::Writeable for Authentication {
    fn write (&self, mut w: &mut Write) -> binary::Result<()> {
        try!(self.login_result.write(w));
        try!(self.reg_stat_id.write(w));
        try!(self.use_day.write(w));
        match self.login_result {
            LoginResult::AuthFail |
            LoginResult::Success => {
                try!(self.account_id.write(w));
                try!(self.gender.write(w));
                try!(self.country_id.write(w));
                try!(self.grade_code.write(w));
                try!(self.username.write(w));
                try!(self.purchase_exp.write(w));
                try!(self.chat_block_reason.write(w));
                try!(self.chat_unblock_date.write(w));
                try!(self.register_date.write(w));
                try!((self.gender as u32).write(w)); // TODO research this more. seems to perform same action, regardless of whether or not it's 10. perhaps client sends a loopback packet?
            },
            LoginResult::Blocked => {
                try!(self.block_reason.write(w));
                try!(self.unblock_date.write(w));
            },
            _ => {}
        }
        Ok(())
    }
}
impl binary::Readable for Authentication {
    fn read(r: &mut Read) -> binary::Result<Authentication> {
        let login_result: LoginResult = try!(Readable::read(r));
        Ok(match login_result {
            LoginResult::AuthFail |
            LoginResult::Success => Authentication {
                login_result: login_result,
                reg_stat_id: try!(Readable::read(r)),
                use_day: try!(Readable::read(r)),
                account_id: try!(Readable::read(r)),
                gender: try!(Readable::read(r)),
                country_id: try!(Readable::read(r)),
                grade_code: try!(Readable::read(r)),
                username: try!(Readable::read(r)),
                purchase_exp: try!(Readable::read(r)),
                chat_block_reason: try!(Readable::read(r)),
                chat_unblock_date: try!(Readable::read(r)),
                register_date: try!(Readable::read(r)),
                block_reason: 0,
                unblock_date: mscore::DateTime::zero_filetime(),
            },
            LoginResult::Blocked => Authentication {
                login_result: login_result,
                reg_stat_id: 0,
                use_day: 0,
                account_id: mscore::AccountId(0),
                gender: mscore::Gender::Male,
                country_id: 0,
                grade_code: 0,
                username: String::new(),
                purchase_exp: 0,
                chat_block_reason: 0,
                chat_unblock_date: mscore::DateTime::zero_filetime(),
                register_date: mscore::DateTime::zero_filetime(),
                block_reason: try!(Readable::read(r)),
                unblock_date: try!(Readable::read(r)),
            },
            _ => Authentication {
                login_result: login_result,
                reg_stat_id: 0,
                use_day: 0,
                account_id: mscore::AccountId(0),
                gender: mscore::Gender::Male,
                country_id: 0,
                grade_code: 0,
                username: String::new(),
                purchase_exp: 0,
                chat_block_reason: 0,
                chat_unblock_date: mscore::DateTime::zero_filetime(),
                register_date: mscore::DateTime::zero_filetime(),
                block_reason: 0,
                unblock_date: mscore::DateTime::zero_filetime(),
            },
        })
    }
}
