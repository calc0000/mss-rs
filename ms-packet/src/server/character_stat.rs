use prelude::*;

#[derive(Clone, Debug)]
pub struct CharacterStat {
    character_id: u32,
    character_name: String,
    gender: u8,
    skin: u8,
    face: u32,
    hair: u32,
    pet_locker_sn: [u64; 3],
    level: u8,
    job: u16,
    str: u16,
    dex: u16,
    int: u16,
    luk: u16,
    hp: u16,
    max_hp: u16,
    mp: u16,
    max_mp: u16,
    ap: u16,
    sp: u16,
    exp: u32,
    fame: u16,
    map: u32,
    portal: u8
}

impl binary::Readable for CharacterStat {
    fn read(r: &mut Read) -> binary::Result<CharacterStat> {
        Ok(CharacterStat {
            character_id: try!(binary::read(r)),
            character_name: try!(binary::read(r)),
            gender: try!(binary::read(r)),
            skin: try!(binary::read(r)),
            face: try!(binary::read(r)),
            hair: try!(binary::read(r)),
            pet_locker_sn: [try!(binary::read(r)), try!(binary::read(r)), try!(binary::read(r))],
            level: try!(binary::read(r)), 
            job: try!(binary::read(r)), 
            str: try!(binary::read(r)), 
            dex: try!(binary::read(r)), 
            int: try!(binary::read(r)), 
            luk: try!(binary::read(r)), 
            hp: try!(binary::read(r)), 
            max_hp: try!(binary::read(r)), 
            mp: try!(binary::read(r)), 
            max_mp: try!(binary::read(r)), 
            ap: try!(binary::read(r)),
            sp: try!(binary::read(r)),
            exp: try!(binary::read(r)),
            fame: try!(binary::read(r)),
            map: try!(binary::read(r)),
            portal: try!(binary::read(r)),
        })
    }
}

impl binary::Writeable for CharacterStat {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        try!(self.character_id.write(w));
        try!(self.character_name.write(w));
        try!(self.gender.write(w));
        try!(self.skin.write(w));
        try!(self.face.write(w));
        try!(self.hair.write(w));
        try!(self.pet_locker_sn[0].write(w));
        try!(self.pet_locker_sn[1].write(w));
        try!(self.pet_locker_sn[2].write(w));
        try!(self.level.write(w));
        try!(self.job.write(w));
        try!(self.str.write(w));
        try!(self.dex.write(w));
        try!(self.int.write(w));
        try!(self.luk.write(w));
        try!(self.hp.write(w));
        try!(self.max_hp.write(w));
        try!(self.mp.write(w));
        try!(self.max_mp.write(w));
        try!(self.ap.write(w));
        try!(self.sp.write(w));
        try!(self.exp.write(w));
        try!(self.fame.write(w));
        try!(self.map.write(w));
        self.portal.write(w)
    }
}
