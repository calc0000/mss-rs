use prelude::*;

#[derive(Clone, Debug)]
pub struct Rank {
    pub world_rank: u32,
    pub world_rank_gap: u32,
    pub job_rank: u32,
    pub job_rank_gap: u32,
}

impl binary::Readable for Rank {
    fn read(r: &mut Read) -> binary::Result<Rank> {
        Ok(Rank {
            world_rank: try!(binary::read(r)),
            world_rank_gap: try!(binary::read(r)),
            job_rank: try!(binary::read(r)),
            job_rank_gap: try!(binary::read(r)),
        })
    }
}

impl binary::Writeable for Rank {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        try!(self.world_rank.write(w));
        try!(self.world_rank_gap.write(w));
        try!(self.job_rank.write(w));
        self.job_rank_gap.write(w)
    }
}

#[derive(Clone, Debug)]
pub struct SelectWorld {
    pub result: u8,
    pub characters: Vec<(Option<Rank>)>,
    pub slot_count: u32,
}

impl binary::Readable for SelectWorld {
    fn read(r: &mut Read) -> binary::Result<SelectWorld> {
        Ok(SelectWorld {
            result: try!(binary::read(r)),
            characters: {
                let mut chars = vec![];
                for _ in 0..try!(binary::read::<u32>(r)) {
                    chars.push(
                    if try!(binary::read(r)) {
                        Some(try!(binary::read::<Rank>(r)))
                    } else {
                        None
                    });
                }
                chars
            },
            slot_count: try!(binary::read(r)),
        })
    }
}

impl binary::Writeable for SelectWorld {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        try!(self.result.write(w));
        try!(self.result.write(w));
        0u32.write(w)
    }
}
