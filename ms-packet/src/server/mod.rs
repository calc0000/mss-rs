use prelude::*;

pub mod connect;
pub mod authentication;
pub mod set_account;
pub mod confirm_eula;
pub mod check_pin;
pub mod update_pin;
pub mod world_info;
pub mod select_world;
pub mod ping;

pub mod character_stat;

pub use self::connect::Connect;
pub use self::authentication::Authentication;
pub use self::set_account::SetAccount;
pub use self::confirm_eula::ConfirmEula;
pub use self::check_pin::CheckPin;
pub use self::update_pin::UpdatePin;
pub use self::world_info::WorldInfo;
pub use self::select_world::SelectWorld;
pub use self::ping::Ping;

use Error;

#[derive(Clone, Copy, Debug)]
pub enum PacketType {
    Authentication = 0,
    SetAccount = 4,
    ConfirmEula = 5,
    CheckPin = 6,
    UpdatePin = 7,
    WorldInfo = 10,
    SelectWorld = 11,
    Ping = 17,
}

impl PacketType {
    fn from_u16 (v: u16) -> Option<PacketType> {
        match v {
            0 => Some(PacketType::Authentication),
            4 => Some(PacketType::SetAccount),
            5 => Some(PacketType::ConfirmEula),
            6 => Some(PacketType::CheckPin),
            7 => Some(PacketType::UpdatePin),
            10 => Some(PacketType::WorldInfo),
            11 => Some(PacketType::SelectWorld),
            17 => Some(PacketType::Ping),
            _ => None,
        }
    }
}

#[derive(Clone, Debug)]
pub enum Packet {
    Connect(Connect),
    Authentication(Authentication),
    SetAccount(SetAccount),
    ConfirmEula(ConfirmEula),
    CheckPin(CheckPin),
    UpdatePin(UpdatePin),
    WorldInfo(WorldInfo),
    SelectWorld(SelectWorld),
    Ping(Ping),
}
impl Packet {
    pub fn write_header (&self, mut w: &mut Write, mscrypto: &mscrypto::Crypto, packet_size: usize) -> binary::Result<()> {
        let mscrypto::Iv(v) = mscrypto.encrypt_iv;
        let version: u16 = ((v[3] as u16) << 8) | (v[2] as u16);
        let version: u16 = version ^ !msconfig::VERSION;
        let psize: u16 = version ^ packet_size as u16;

        try!(binary::write(w, &version));
        try!(binary::write(w, &psize));
        Ok(())
    }

    pub fn read (mut r: &mut Read) -> Result<Packet, Error> {
        let header: super::Header = try!(binary::Readable::read(r));
        let super::Header(value) = header;
        let packet_type = PacketType::from_u16(value);
        match packet_type {
            Some(s) => Ok(match s {
                PacketType::Authentication => Packet::Authentication(try!(binary::read(r))),
                PacketType::SetAccount => Packet::SetAccount(try!(binary::read(r))),
                PacketType::ConfirmEula => Packet::ConfirmEula(try!(binary::read(r))),
                PacketType::CheckPin => Packet::CheckPin(try!(binary::read(r))),
                PacketType::UpdatePin => Packet::UpdatePin(try!(binary::read(r))),
                PacketType::WorldInfo => Packet::WorldInfo(try!(binary::read(r))),
                PacketType::SelectWorld => Packet::SelectWorld(try!(binary::read(r))),
                PacketType::Ping => Packet::Ping(try!(binary::read(r))),
            }),
            None => Err(Error::UnknownPacketType(value)),
        }
    }
}

impl binary::Writeable for Packet {
    fn write (&self, mut w: &mut Write) -> binary::Result<()> {
        match self {
            &Packet::Connect(ref p) => {
                //let header = super::Header(0 as u16);

                binary::write(w, p)
            }
            &Packet::Authentication(ref p) => {
                let header = super::Header(PacketType::Authentication as u16);

                try!(binary::write(w, &header));
                binary::write(w, p)
            },
            &Packet::SetAccount(ref p) => {
                let header = super::Header(PacketType::SetAccount as u16);
                try!(binary::write(w, &header));
                binary::write(w, p)
            },
            &Packet::ConfirmEula(ref p) => {
                let header = super::Header(PacketType::ConfirmEula as u16);
                try!(binary::write(w, &header));
                binary::write(w, p)
            },
            &Packet::CheckPin(ref p) => {
                let header = super::Header(PacketType::CheckPin as u16);
                try!(binary::write(w, &header));
                binary::write(w, p)
            },
            &Packet::UpdatePin(ref p) => {
                let header = super::Header(PacketType::UpdatePin as u16);
                try!(binary::write(w, &header));
                binary::write(w, p)
            },
            &Packet::WorldInfo(ref p) => {
                let header = super::Header(PacketType::WorldInfo as u16);
                try!(binary::write(w, &header));
                binary::write(w, p)
            },
            &Packet::SelectWorld(ref p) => {
                let header = super::Header(PacketType::SelectWorld as u16);
                try!(binary::write(w, &header));
                binary::write(w, p)
            },
            &Packet::Ping(ref p) => {
                let header = super::Header(PacketType::Ping as u16);
                try!(binary::write(w, &header));
                binary::write(w, p)
            },
        }
    }
}
