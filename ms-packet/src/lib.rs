extern crate ms_config as msconfig;
extern crate ms_core as mscore;
extern crate ms_crypto as mscrypto;

mod prelude {
    pub use mscore::prelude::*;
    pub use msconfig;
    pub use mscore;
    pub use mscrypto;
}
pub mod error;
pub mod header;
pub mod server;
pub mod client;

pub use error::Error;
pub use header::Header;

#[cfg(test)]
mod test {
    use prelude::*;

    use std::io::Cursor;

    use server;
    use client;

    #[test]
    fn test_connect_packet () {
        let buf: &[u8] = &[
            0x3B, 0x00,
            0x01, 0x00, 0x31,
            0x01, 0x02, 0x03, 0x04,
            0x10, 0x11, 0x12, 0x13,
            0x08,
            ];
        let mut inf = Cursor::new(buf);
        let cp: super::server::Connect = binary::Readable::read(&mut inf).unwrap();
        assert!(cp.version == mscore::Version(0x3B));
        assert!(&cp.sub_version[..] == "1");
        assert!(cp.receive_iv == mscrypto::Iv([1, 2, 3, 4]));
        assert!(cp.send_iv == mscrypto::Iv([16, 17, 18, 19]));
        assert!(cp.locale == mscore::Locale(8));

        let mut outf = Vec::<u8>::new();
        cp.write(&mut outf).unwrap();
        assert!(buf == &outf[..]);
    }

    #[test]
    fn test_auth_packet () {
        let auth = server::Packet::Authentication(server::Authentication {
            unk1: 0,
            unk2: 0,
            account_id: mscore::AccountId(12),
            status: server::authentication::Status::Gender(mscore::Gender::Male),
            unk3: 0,
            username: "abcde".to_string(),
            unk4: 0,
            unk5: 0,
            unk6: 0,
            unk7: 0,
            unk8: 0,
        });
        assert!(auth.get_size() == 42 + 2, "size is {}", auth.get_size());
    }
}
