use prelude::*;

#[derive(Clone, Debug)]
pub struct Authentication {
    pub username: String,
    pub password: String,
    pub machine_id: [u8; 16],
    pub game_room_code: u32,
    pub game_start_mode: u8,
    pub admin_client: bool, // this is hardcoded as 0 in the client
    pub unk: u8 // this is hardcoded as 0 in the client
}

impl binary::Writeable for Authentication {
    fn write (&self, mut w: &mut Write) -> binary::Result<()> {
        try!(self.username.write(w));
        try!(self.password.write(w));
        try!(w.write_all(&self.machine_id));
        try!(self.game_room_code.write(w));
        try!(self.game_start_mode.write(w));
        try!(self.admin_client.write(w));
        try!(self.unk.write(w));
        Ok(())
    }
}
impl binary::Readable for Authentication {
    fn read (mut r: &mut Read) -> binary::Result<Authentication> {
        Ok(Authentication {
            username: try!(binary::Readable::read(r)),
            password: try!(binary::Readable::read(r)),
            machine_id: {
                let mut ret: [u8; 16] = [0; 16];
                for b in &mut ret {
                    *b = try!(binary::Readable::read(r))
                }
                ret
            },
            game_room_code: try!(binary::Readable::read(r)),
            game_start_mode: try!(binary::Readable::read(r)),
            admin_client: try!(binary::Readable::read(r)),
            unk: try!(binary::Readable::read(r))
        })
    }
}
