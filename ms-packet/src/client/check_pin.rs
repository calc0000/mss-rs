use prelude::*;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum PinAction {
    Cancel = 0,
    Login = 1,
    Update = 2
}
impl PinAction {
    fn from_u8 (pin_action: u8) -> Option<PinAction> {
        match pin_action {
            0 => Some(PinAction::Cancel),
            1 => Some(PinAction::Login),
            2 => Some(PinAction::Update),
            _ => None
        }
    }
}
impl binary::Readable for PinAction {
    fn read (r: &mut Read) -> binary::Result<PinAction> {
        Ok(PinAction::from_u8(try!(binary::read(r))).unwrap())
    }
}
impl binary::Writeable for PinAction {
    fn write (&self, mut w: &mut Write) -> binary::Result<()> {
        let pin_action: u8 = match *self {
            PinAction::Cancel => 0,
            PinAction::Login => 1,
            PinAction::Update => 2,
        };
        binary::write(w, &pin_action)
    }
}

#[derive(Clone, Debug)]
pub struct CheckPin {
    pub pin_action: PinAction,
    pub init: bool,
    pub account_id: mscore::AccountId,
    pub pin: String
}
impl binary::Readable for CheckPin {
    fn read (mut r: &mut Read) -> binary::Result<CheckPin> {
        let pin_action: PinAction = try!(binary::read(r));
        Ok(if pin_action != PinAction::Cancel {
            CheckPin {
                pin_action: pin_action,
                init: try!(binary::read(r)),
                account_id: try!(binary::read(r)),
                pin: try!(binary::read(r))
            }
        } else {
            CheckPin {
                pin_action: PinAction::Cancel,
                init: false,
                account_id: mscore::AccountId(0),
                pin: String::new()
            }
        })
    }
}
impl binary::Writeable for CheckPin {
    fn write (&self, mut w: &mut Write) -> binary::Result<()> {
        try!(self.pin_action.write(w));
        if self.pin_action != PinAction::Cancel {
            try!(self.init.write(w));
            try!(self.account_id.write(w));
            try!(self.pin.write(w));
        }
        Ok(())
    }
}
