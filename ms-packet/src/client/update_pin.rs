use prelude::*;

#[derive(Clone, Debug)]
pub struct UpdatePin {
    pub ok: bool,
    pub pin: String,
}
impl binary::Readable for UpdatePin {
    fn read(mut r: &mut Read) -> binary::Result<UpdatePin> {
        let ok: bool = try!(binary::read(r)); // false if user press cancel
        Ok(UpdatePin {
            ok: ok,
            pin: if ok {
                    try!(binary::read(r))
                } else {
                    String::new()
                },
        })
    }
}
impl binary::Writeable for UpdatePin {
    fn write(&self, mut w: &mut Write) -> binary::Result<()> {
        try!(self.ok.write(w));
        if self.ok {
            try!(self.pin.write(w));
        }
        Ok(())
    }
}
