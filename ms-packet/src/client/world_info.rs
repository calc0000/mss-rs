use prelude::*;

#[derive(Clone, Debug)]
pub struct WorldInfo;

impl binary::Readable for WorldInfo {
    fn read(_: &mut Read) -> binary::Result<WorldInfo> {
        Ok(WorldInfo)
    }
}
impl binary::Writeable for WorldInfo {
    fn write(&self, _: &mut Write) -> binary::Result<()> {
        Ok(())
    }
}
