use prelude::*;

use Error;

pub mod authentication;
pub mod check_user_limit;
pub mod confirm_eula;
pub mod set_gender;
pub mod check_pin;
pub mod update_pin;
pub mod world_info;
pub mod pong;
pub mod exception_log;
pub mod security;

pub use self::authentication::Authentication;
pub use self::check_user_limit::CheckUserLimit;
pub use self::confirm_eula::ConfirmEula;
pub use self::set_gender::SetGender;
pub use self::check_pin::CheckPin;
pub use self::update_pin::UpdatePin;
pub use self::world_info::WorldInfo;
pub use self::pong::Pong;
pub use self::exception_log::ExceptionLog;
pub use self::security::Security;

#[derive(Clone, Copy, Debug)]
pub enum PacketType {
    Authentication = 1,
    CheckUserLimit = 6,
    ConfirmEula = 7,
    SetGender = 8,
    CheckPin   = 9,
    UpdatePin = 10,
    WorldInfo = 11,
    Pong = 24,
    ExceptionLog = 25,
    Security = 26,
}

impl PacketType {
    fn from_u16 (v: u16) -> Option<PacketType> {
        match v {
            1 => Some(PacketType::Authentication),
            6 => Some(PacketType::CheckUserLimit),
            7 => Some(PacketType::ConfirmEula),
            8 => Some(PacketType::SetGender),
            9 => Some(PacketType::CheckPin),
            10 => Some(PacketType::UpdatePin),
            11 => Some(PacketType::WorldInfo),
            24 => Some(PacketType::Pong),
            25 => Some(PacketType::ExceptionLog),
            26 => Some(PacketType::Security),
            _ => None,
        }
    }
}

#[derive(Clone, Debug)]
pub enum Packet {
    Authentication(Authentication),
    CheckUserLimit(CheckUserLimit),
    ConfirmEula(ConfirmEula),
    SetGender(SetGender),
    CheckPin(CheckPin),
    UpdatePin(UpdatePin),
    WorldInfo(WorldInfo),
    Pong(Pong),
    ExceptionLog(ExceptionLog),
    Security(Security),
}
impl Packet {
    pub fn write_header (&self, mut w: &mut Write, mscrypto: &mscrypto::Crypto, packet_len: usize) -> binary::Result<()> {
        let mscrypto::Iv(v) = mscrypto.encrypt_iv;
        let version: u16 = ((v[3] as u16) << 8) | (v[2] as u16);
        let version: u16 = version ^ !msconfig::VERSION;
        let psize: u16 = version ^ packet_len as u16;

        try!(binary::write(w, &version));
        try!(binary::write(w, &psize));
        Ok(())
    }

    pub fn read (mut r: &mut Read) -> Result<Packet, Error> {
        let header: super::Header = try!(binary::Readable::read(r));
        let super::Header(value) = header;
        let packet_type = PacketType::from_u16(value);
        match packet_type {
            Some(s) => Ok(match s {
                PacketType::Authentication => Packet::Authentication(try!(binary::read(r))),
                PacketType::CheckUserLimit => Packet::CheckUserLimit(try!(binary::read(r))),
                PacketType::ConfirmEula => Packet::ConfirmEula(try!(binary::read(r))),
                PacketType::SetGender => Packet::SetGender(try!(binary::read(r))),
                PacketType::CheckPin   => Packet::CheckPin(try!(binary::read(r))),
                PacketType::UpdatePin => Packet::UpdatePin(try!(binary::read(r))),
                PacketType::WorldInfo => Packet::WorldInfo(try!(binary::read(r))),
                PacketType::Pong => Packet::Pong(try!(binary::read(r))),
                PacketType::ExceptionLog => Packet::ExceptionLog(try!(binary::read(r))),
                PacketType::Security   => Packet::Security(try!(binary::read(r))),
            }),
            None => Err(Error::UnknownPacketType(value)),
        }
    }
}
impl binary::Writeable for Packet {
    fn write (&self, mut w: &mut Write) -> binary::Result<()> {
        match self {
            &Packet::Authentication(ref p) => {
                try!(binary::write(w, &super::Header(PacketType::Authentication as u16)));
                p.write(w)
            },
            &Packet::CheckUserLimit(ref p) => {
                try!(binary::write(w, &super::Header(PacketType::CheckUserLimit as u16)));
                p.write(w)
            },
            &Packet::ConfirmEula(ref p) => {
                try!(binary::write(w, &super::Header(PacketType::ConfirmEula as u16)));
                p.write(w)
            },
            &Packet::SetGender(ref p) => {
                try!(binary::write(w, &super::Header(PacketType::SetGender as u16)));
                p.write(w)
            },
            &Packet::CheckPin(ref p) => {
                try!(binary::write(w, &super::Header(PacketType::CheckPin as u16)));
                p.write(w)
            },
            &Packet::UpdatePin(ref p) => {
                try!(binary::write(w, &super::Header(PacketType::UpdatePin as u16)));
                p.write(w)
            },
            &Packet::WorldInfo(ref p) => {
                try!(binary::write(w, &super::Header(PacketType::WorldInfo as u16)));
                p.write(w)
            },
            &Packet::Pong(ref p) => {
                try!(binary::write(w, &super::Header(PacketType::Pong as u16)));
                p.write(w)
            },
            &Packet::ExceptionLog(ref p) => {
                try!(binary::write(w, &super::Header(PacketType::ExceptionLog as u16)));
                p.write(w)
            },
            &Packet::Security(ref p) => {
                try!(binary::write(w, &super::Header(PacketType::Security as u16)));
                p.write(w)
            },
        }
    }
}
