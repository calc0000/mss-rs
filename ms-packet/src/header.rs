use mscore::prelude::*;

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Header(pub u16);

impl binary::Writeable for Header {
    fn write (&self, mut w: &mut Write) -> binary::Result<()> {
        let &Header(v) = self;
        binary::write(w, &v)
    }
}
impl binary::Readable for Header {
    fn read (mut r: &mut Read) -> binary::Result<Header> {
        Ok(Header(try!(binary::read(r))))
    }
}
