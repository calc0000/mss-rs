extern crate rand;
extern crate ms_core as mscore;

pub mod crc32;

use mscore::prelude::*;

use std::fmt;
use rand::{Rand, Rng, random};

#[derive(Copy)]
pub struct Iv (pub [u8; 4]);
impl binary::Writeable for Iv {
    fn write (&self, w: &mut Write) -> binary::Result<()> {
        let &Iv(v) = self;
        try!(binary::write(w, &v[0]));
        try!(binary::write(w, &v[1]));
        try!(binary::write(w, &v[2]));
        try!(binary::write(w, &v[3]));
        Ok(())
    }
}
impl binary::Readable for Iv {
    fn read (r: &mut Read) -> binary::Result<Iv> {
        let v1 = try!(binary::read(r));
        let v2 = try!(binary::read(r));
        let v3 = try!(binary::read(r));
        let v4 = try!(binary::read(r));
        Ok(Iv([v1, v2, v3, v4]))
    }
}
impl fmt::Debug for Iv {
    fn fmt (&self, f: &mut fmt::Formatter) -> fmt::Result {
        let &Iv(v) = self;
        write!(f, "Iv([{:02X}, {:02X}, {:02X}, {:02X}])", v[0], v[1], v[2], v[3])
    }
}
impl PartialEq for Iv {
    fn eq (&self, other: &Iv) -> bool {
        let &Iv(x) = self;
        let &Iv(y) = other;
        x[0] == y[0] &&
            x[1] == y[1] &&
            x[2] == y[2] &&
            x[3] == y[3]
    }
}
impl Rand for Iv {
    fn rand<R: Rng> (_: &mut R) -> Iv {
        Iv([
           random::<u8>(),
           random::<u8>(),
           random::<u8>(),
           random::<u8>(),
           ])
    }
}
impl Clone for Iv {
    fn clone (&self) -> Iv {
        let &Iv(v) = self;
        Iv([v[0], v[1], v[2], v[3]])
    }
}

#[allow(missing_copy_implementations)]
#[derive(Clone)]
pub struct Crypto {
    pub encrypt_iv: Iv,
    pub decrypt_iv: Iv,
}
impl Crypto {
    pub fn new (encrypt_iv: Option<Iv>, decrypt_iv: Option<Iv>) -> Crypto {
        Crypto {
            encrypt_iv: match encrypt_iv {
                            Some(s) => s,
                            None => random::<Iv>(),
                        },
            decrypt_iv: match decrypt_iv {
                            Some(s) => s,
                            None => random::<Iv>(),
                        },
        }
    }
    pub fn decrypt (&mut self, buf: &mut [u8]) {
        self.decrypt_aes(buf);
        maple::next_iv(&mut self.decrypt_iv);
        maple::decrypt(buf);
    }
    pub fn encrypt (&mut self, buf: &mut [u8]) {
        maple::encrypt(buf);

        let mut first = true;
        let len = buf.len();
        let mut pos = 0usize;

        while len > pos {
            if len > pos + 1460 - if first {4} else {0} {
                let size = 1460 - if first {4} else {0};
                self.encrypt_aes(&mut buf[pos..size]);
            } else {
                self.encrypt_aes(&mut buf[pos..len - pos])
            }

            pos += 1460 - if first {4} else {0};
            if first {
                first = false;
            }
        }

        maple::next_iv(&mut self.encrypt_iv);
    }
}

impl Crypto {
    fn decrypt_aes (&self, buf: &mut [u8]) {
        let len = buf.len();
        aes::decrypt_ofb(buf, &self.decrypt_iv, len);
    }
    fn encrypt_aes (&self, buf: &mut [u8]) {
        let len = buf.len();
        aes::decrypt_ofb(buf, &self.encrypt_iv, len);
    }
}

#[cfg(test)]
mod test {
    use mscore::prelude::*;

    #[test]
    fn test_rotate_left () {
        let v: u8 = 13; // 0b1101
        assert!(v.rotate_left(3) == 104);  // 0b1101000
        assert!(v.rotate_left(11) == 104); // 0b1101000
        assert!(v.rotate_left(19) == 104); // 0b1101000

        let v: u8 = 208; // 0b11010000
        assert!(v.rotate_left(4) == 13);   // 0b1101
        assert!(v.rotate_left(12) == 13);  // 0b1101
        assert!(v.rotate_left(20) == 13);  // 0b1101
    }
    #[test]
    fn test_rotate_right () {
        let v: u8 = 13; // 0b1101
        assert!(v.rotate_right(4) == 208);  // 0b1101000
        assert!(v.rotate_right(12) == 208); // 0b1101000
        assert!(v.rotate_right(20) == 208); // 0b1101000

        let v: u8 = 208; // 0b11010000
        assert!(v.rotate_right(4) == 13);   // 0b1101
        assert!(v.rotate_right(12) == 13);  // 0b1101
        assert!(v.rotate_right(20) == 13);  // 0b1101
    }
    #[test]
    fn test_decrypt () {
        let mut encrypted: [u8; 43] = [
            0x0B, 0x30, 0x2C, 0x30,
            0xD5, 0xFD, 0x86, 0x01,
            0xF1, 0xE0, 0xFD, 0x1D,
            0xEC, 0xA5, 0x3C, 0x29,
            0x84, 0xB3, 0x65, 0xD2,
            0x01, 0xF8, 0x38, 0x77,
            0x15, 0xEE, 0xAF, 0x22,
            0x12, 0x6C, 0x49, 0x96,
            0x19, 0x49, 0x02, 0xD6,
            0xCD, 0x34, 0xE0, 0x3F,
            0x0E, 0x20, 0x50,
            ];
        let decrypted: [u8 ;43] = [
            0x0B, 0x30, 0x2C, 0x30,
            0x01, 0x00, 0x05, 0x00,
            0x61, 0x62, 0x63, 0x64,
            0x65, 0x05, 0x00, 0x61,
            0x62, 0x63, 0x64, 0x65,
            0x00, 0x0C, 0x29, 0x65,
            0x1B, 0xBA, 0x9B, 0xC9,
            0xE3, 0xC4, 0x00, 0x00,
            0x00, 0x00, 0x02, 0x77,
            0x00, 0x00, 0x00, 0x00,
            0x02, 0x00, 0x00,
            ];
        let mut crypto = super::Crypto::new(
            Some(super::Iv([0x30, 0x30, 0x30, 0x30])), Some(super::Iv([0x30, 0x30, 0x30, 0x30]))
            );

        crypto.decrypt(&mut encrypted[4..]);
        for i in 0..encrypted.len() {
            assert!(
                encrypted[i] == decrypted[i],
                "Failed at char {}: {:02X} vs {:02X}",
                i, encrypted[i], decrypted[i]
                );
        }
    }
}

mod maple {
    pub fn next_iv (iv: &mut super::Iv) {
        let values: [u8; 256] = [
            0xEC, 0x3F, 0x77, 0xA4, 0x45, 0xD0, 0x71, 0xBF, 0xB7, 0x98, 0x20, 0xFC,
            0x4B, 0xE9, 0xB3, 0xE1, 0x5C, 0x22, 0xF7, 0x0C,	0x44, 0x1B, 0x81, 0xBD, 0x63, 0x8D, 0xD4, 0xC3,
            0xF2, 0x10, 0x19, 0xE0, 0xFB, 0xA1, 0x6E, 0x66,	0xEA, 0xAE, 0xD6, 0xCE, 0x06, 0x18, 0x4E, 0xEB,
            0x78, 0x95, 0xDB, 0xBA, 0xB6, 0x42, 0x7A, 0x2A, 0x83, 0x0B, 0x54, 0x67, 0x6D, 0xE8, 0x65, 0xE7,
            0x2F, 0x07, 0xF3, 0xAA, 0x27, 0x7B, 0x85, 0xB0,	0x26, 0xFD, 0x8B, 0xA9, 0xFA, 0xBE, 0xA8, 0xD7,
            0xCB, 0xCC, 0x92, 0xDA, 0xF9, 0x93, 0x60, 0x2D,	0xDD, 0xD2, 0xA2, 0x9B, 0x39, 0x5F, 0x82, 0x21,
            0x4C, 0x69, 0xF8, 0x31, 0x87, 0xEE, 0x8E, 0xAD, 0x8C, 0x6A, 0xBC, 0xB5, 0x6B, 0x59, 0x13, 0xF1,
            0x04, 0x00, 0xF6, 0x5A, 0x35, 0x79, 0x48, 0x8F,	0x15, 0xCD, 0x97, 0x57, 0x12, 0x3E, 0x37, 0xFF,
            0x9D, 0x4F, 0x51, 0xF5, 0xA3, 0x70, 0xBB, 0x14,	0x75, 0xC2, 0xB8, 0x72, 0xC0, 0xED, 0x7D, 0x68,
            0xC9, 0x2E, 0x0D, 0x62, 0x46, 0x17, 0x11, 0x4D,	0x6C, 0xC4, 0x7E, 0x53, 0xC1, 0x25, 0xC7, 0x9A,
            0x1C, 0x88, 0x58, 0x2C, 0x89, 0xDC, 0x02, 0x64,	0x40, 0x01, 0x5D, 0x38, 0xA5, 0xE2, 0xAF, 0x55,
            0xD5, 0xEF, 0x1A, 0x7C, 0xA7, 0x5B, 0xA6, 0x6F,	0x86, 0x9F, 0x73, 0xE6, 0x0A, 0xDE, 0x2B, 0x99,
            0x4A, 0x47, 0x9C, 0xDF, 0x09, 0x76, 0x9E, 0x30,	0x0E, 0xE4, 0xB2, 0x94, 0xA0, 0x3B, 0x34, 0x1D,
            0x28, 0x0F, 0x36, 0xE3, 0x23, 0xB4, 0x03, 0xD8, 0x90, 0xC8, 0x3C, 0xFE, 0x5E, 0x32, 0x24, 0x50,
            0x1F, 0x3A, 0x43, 0x8A, 0x96, 0x41, 0x74, 0xAC,	0x52, 0x33, 0xF0, 0xD9, 0x29, 0x80, 0xB1, 0x16,
            0xD3, 0xAB, 0x91, 0xB9, 0x84, 0x7F, 0x61, 0x1E,	0xCF, 0xC5, 0xD1, 0x56, 0x3D, 0xCA, 0xF4, 0x05,
            0xC6, 0xE5, 0x08, 0x49
            ];
        let &mut super::Iv(ref mut v) = iv;
        let mut new_iv: [u8; 4] = [0xF2, 0x53, 0x50, 0xC6];
        for i in 0..4 {
            let mut a: u8 = new_iv[1];
            let mut b: u8 = values[a as usize];
            let c: u8 = v[i];

            b = b.wrapping_sub(c);
            new_iv[0] = new_iv[0].wrapping_add(b);

            b = new_iv[2];
            b ^= values[c as usize];
            a = a.wrapping_sub(b);
            new_iv[1] = a;

            a = new_iv[3];
            b = a;
            a = a.wrapping_sub(new_iv[0]);
            b = values[b as usize];
            b = b.wrapping_add(c);
            b ^= new_iv[2];
            new_iv[2] = b;

            a = a.wrapping_add(values[c as usize]);
            new_iv[3] = a;

            let mut x =
                (new_iv[0] as usize) |
                ((new_iv[1] as usize) << 8) |
                ((new_iv[2] as usize) << 16) |
                ((new_iv[3] as usize) << 24);
            let mut y = x >> 0x1D;
            x <<= 3;
            y |= x;

            new_iv[0] = y as u8;
            new_iv[1] = (y >> 8) as u8;
            new_iv[2] = (y >> 16) as u8;
            new_iv[3] = (y >> 24) as u8;
        }
        v[0] = new_iv[0];
        v[1] = new_iv[1];
        v[2] = new_iv[2];
        v[3] = new_iv[3];
    }
    pub fn decrypt (buf: &mut [u8]) {
        let size = buf.len();
        for _ in 0..3 {
            let mut a: u8;
            let mut b: u8 = 0;

            let mut c: u8;
            for j in (0..size).rev() {
                c = buf[j];
                c = c.rotate_left(3);
                c = c ^ 0x13;
                a = c;
                c = c ^ b;
                c = c.wrapping_sub((j as u8).wrapping_add(1));
                c = c.rotate_right(4);
                b = a;
                buf[j] = c;
            }

            let mut a: u8;
            let mut b: u8 = 0;

            let mut c: u8;
            for j in (0..size).rev() {
                c = buf[size - j - 1];
                c = c.wrapping_sub(0x48);
                c = c ^ 0xFF;
                c = c.rotate_left((j as u32).wrapping_add(1));
                a = c;
                c = c ^ b;
                c = c.wrapping_sub((j as u8).wrapping_add(1));
                c = c.rotate_right(3);
                b = a;
                buf[size - j - 1] = c;
            }
        }
    }
    pub fn encrypt (buf: &mut [u8]) {
        let size = buf.len();
        for _ in 0..3 {
            let mut a: u8 = 0;
            let mut c: u8;
            
            for j in (0..size).rev() {
                c = buf[size - j - 1];
                c = c.rotate_left(3);
                c = c.wrapping_add((j as u8).wrapping_add(1));
                c = c ^ a;
                a = c;
                c = c.rotate_right((j as u32).wrapping_add(1));
                c = c ^ 0xFF;
                c = c.wrapping_add(0x48);
                buf[size - j - 1] = c;
            }

            let mut a: u8 = 0;
            let mut c: u8;
            for j in (0..size).rev() {
                c = buf[j];
                c = c.rotate_left(4);
                c = c.wrapping_add(j as u8 + 1);
                c = c ^ a;
                a = c;
                c = c ^ 0x13;
                c = c.rotate_right(3);
                buf[j] = c;
            }
        }
    }
}

mod aes {
    use std::cmp::min;
    const SBOX: [u8; 256] = [
        //0	 1	2	  3	 4	5	 6	 7	  8	9	 A	  B	C	 D	 E	 F
        0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76, //0
        0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0, //1
        0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15, //2
        0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75, //3
        0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84, //4
        0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf, //5
        0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8, //6
        0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2, //7
        0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73, //8
        0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb, //9
        0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79, //A
        0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08, //B
        0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a, //C
        0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e, //D
        0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf, //E
        0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16, // F
        ];
    const RCON: [u8; 255] = [
        0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8,
        0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3,
        0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f,
        0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d,
        0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab,
        0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d,
        0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25,
        0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01,
        0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d,
        0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa,
        0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a,
        0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02,
        0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a,
        0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef,
        0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94,
        0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04,
        0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f,
        0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5,
        0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33,
        0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb,
        ];

    fn rotate (word: &mut [u8]) {
        let c = word[0];
        word[0] = word[1];
        word[1] = word[2];
        word[2] = word[3];
        word[3] = c;
    }
    fn core (word: &mut [u8], iteration: u8) {
        rotate(word);
        word[0] = SBOX[word[0] as usize];
        word[1] = SBOX[word[1] as usize];
        word[2] = SBOX[word[2] as usize];
        word[3] = SBOX[word[3] as usize];
        word[0] = word[0] ^ RCON[iteration as usize];
    }
    fn expand_key (expanded_key: &mut [u8; 240], key: &[u8; 32]) {
        let expanded_key_size = 240;

        for i in 0..key.len() {
            expanded_key[i] = key[i];
        }

        let mut current_size = key.len();
        let mut rcon_iteration = 1;

        while current_size < expanded_key_size {
            let mut t: [u8; 4] = [0, 0, 0, 0];

            for i in 0..4 {
                t[i] = expanded_key[current_size - 4 + i];
            }

            if current_size % key.len() == 0 {
                core(&mut t, rcon_iteration as u8);
                rcon_iteration += 1;
            }

            if key.len() == 32 && ((current_size % key.len()) == 16) {
                for i in 0..4 {
                    t[i] = SBOX[t[i] as usize];
                }
            }

            for i in 0..4 {
                expanded_key[current_size] = expanded_key[current_size - key.len()] ^ t[i];
                current_size += 1;
            }
        }
    }
    fn sub_bytes (state: &mut [u8]) {
        for i in 0..16 {
            state[i] = SBOX[state[i] as usize];
        }
    }
    fn shift_row (state: &mut [u8], count: u8) {
        for _ in 0..count {
            let c = state[0];
            state[0] = state[1];
            state[1] = state[2];
            state[2] = state[3];
            state[3] = c;
        }
    }
    fn shift_rows (state: &mut [u8]) {
        shift_row(&mut state[0.. 4],   0);
        shift_row(&mut state[4.. 8],   1);
        shift_row(&mut state[8.. 12],  2);
        shift_row(&mut state[12.. 16], 3);
    }
    fn add_round_key (state: &mut [u8], round_key: &[u8]) {
        state[ 0] = state[ 0] ^ round_key[ 0];
        state[ 1] = state[ 1] ^ round_key[ 1];
        state[ 2] = state[ 2] ^ round_key[ 2];
        state[ 3] = state[ 3] ^ round_key[ 3];
        state[ 4] = state[ 4] ^ round_key[ 4];
        state[ 5] = state[ 5] ^ round_key[ 5];
        state[ 6] = state[ 6] ^ round_key[ 6];
        state[ 7] = state[ 7] ^ round_key[ 7];
        state[ 8] = state[ 8] ^ round_key[ 8];
        state[ 9] = state[ 9] ^ round_key[ 9];
        state[10] = state[10] ^ round_key[10];
        state[11] = state[11] ^ round_key[11];
        state[12] = state[12] ^ round_key[12];
        state[13] = state[13] ^ round_key[13];
        state[14] = state[14] ^ round_key[14];
        state[15] = state[15] ^ round_key[15];
    }
    fn galois_multiplication (mut a: u8, mut b: u8) -> u8 {
        let mut p: u8 = 0;

        for _ in 0..16 {
            if b & 1 == 1 {
                p ^= a;
            }
            let hi_bit = a & 0x80;
            a <<= 1;
            if hi_bit == 0x80 {
                a ^= 0x1b;
            }
            b >>= 1;
        }

        p
    }
    fn mix_column (column: &mut [u8]) {
        let mut copy: [u8; 4] = [0, 0, 0, 0];
        copy[0] = column[0];
        copy[1] = column[1];
        copy[2] = column[2];
        copy[3] = column[3];

        column[0] =
            galois_multiplication(copy[0], 2) ^
            galois_multiplication(copy[3], 1) ^
            galois_multiplication(copy[2], 1) ^
            galois_multiplication(copy[1], 3);
        column[1] =
            galois_multiplication(copy[1], 2) ^
            galois_multiplication(copy[0], 1) ^
            galois_multiplication(copy[3], 1) ^
            galois_multiplication(copy[2], 3);
        column[2] =
            galois_multiplication(copy[2], 2) ^
            galois_multiplication(copy[1], 1) ^
            galois_multiplication(copy[0], 1) ^
            galois_multiplication(copy[3], 3);
        column[3] =
            galois_multiplication(copy[3], 2) ^
            galois_multiplication(copy[2], 1) ^
            galois_multiplication(copy[1], 1) ^
            galois_multiplication(copy[0], 3);
    }
    fn mix_columns (state: &mut [u8]) {
        let mut column: [u8; 4] = [0, 0, 0, 0];

        for i in 0..4 {
            for j in 0..4 {
                column[j] = state[j * 4 + i];
            }
            mix_column(&mut column);
            for j in 0..4 {
                state[j * 4 + i] = column[j];
            }
        }
    }
    fn aes_round (state: &mut [u8], round_key: &mut [u8]) {
        sub_bytes(state);
        shift_rows(state);
        mix_columns(state);
        add_round_key(state, round_key);
    }

    fn create_round_key (expanded_key: &[u8], round_key: &mut [u8]) {
        for i in 0..4 {
            for j in 0..4 {
                round_key[i + j * 4] = expanded_key[i * 4 + j];
            }
        }
    }

    fn aes_main (state: &mut [u8], expanded_key: &[u8], num_rounds: usize) {
        let mut round_key: [u8; 16] = [0; 16];

        create_round_key(expanded_key, &mut round_key);
        add_round_key(state, &round_key);

        for i in 1..num_rounds {
            create_round_key(&expanded_key[i * 16..], &mut round_key);
            aes_round(state, &mut round_key);
        }

        create_round_key(&expanded_key[16 * num_rounds..], &mut round_key);
        sub_bytes(state);
        shift_rows(state);
        add_round_key(state, &mut round_key);
    }
    fn aes_encrypt (input: &[u8], output: &mut [u8], key: &[u8; 32]) {
        let num_rounds = 14;
        let mut expanded_key: [u8; 240] = [0; 240];
        let mut block: [u8; 16] = [0; 16];

        for i in 0..4 {
            for j in 0..4 {
                block[i + (j * 4)] = input[(i * 4) + j];
            }
        }

        expand_key(&mut expanded_key, key);
        aes_main(&mut block, &expanded_key, num_rounds);

        for i in 0..4 {
            for j in 0..4 {
                output[(i * 4) + j] = block[i + (j * 4)];
            }
        }
    }

    pub fn decrypt_ofb (buffer: &mut [u8], short_iv: &super::Iv, size: usize) {
        let mut input: [u8; 16] = [0; 16];
        let mut output: [u8; 16] = [0; 16];
        let mut plaintext: [u8; 16] = [0; 16];
        let mut iv: [u8; 16] = [0; 16];

        let &super::Iv(vec) = short_iv;
        for i in 0..iv.len() {
            iv[i] = vec[i % 4];
        }

        let key: [u8; 32] = [
            0x13, 0x00, 0x00, 0x00,
            0x08, 0x00, 0x00, 0x00,
            0x06, 0x00, 0x00, 0x00,
            0xB4, 0x00, 0x00, 0x00,
            0x1B, 0x00, 0x00, 0x00,
            0x0F, 0x00, 0x00, 0x00,
            0x33, 0x00, 0x00, 0x00,
            0x52, 0x00, 0x00, 0x00,
            ];
        let mut first_round = true;

        let mut len = buffer.len();
        for j in 0..size / 16 + 1 {
            if first_round {
                aes_encrypt(&iv, &mut output, &key);
                first_round = false;
            } else {
                aes_encrypt(&input, &mut output, &key);
            }

            for i in 0..min(16, len) {
                plaintext[i] = output[i] ^ buffer[j * 16 + i];
            }
            if len > 16 {
                len -= 16;
            } else {
                len = 0;
            }

            if j == size/16 {
                for k in 0..size % 16 {
                    buffer[(j * 16) + k] = plaintext[k];
                }
            } else {
                for k in 0..16 {
                    buffer[(j * 16) + k] = plaintext[k];
                }
            }

            for k in 0..16 {
                input[k] = output[k];
            }
        }
    }
}
