INSERT INTO users (
    username, pass_hash,
    pin, gender
) VALUES (
    'admin', '$2a$12$aktwRx5l.6h0aoeqzAcBfesdRdwgWmkTdswVca1OMkCcs3TELo/1e',
    '1234', 'Male'
);
INSERT INTO users (
    username, pass_hash,
    pin, gender, block_reason, unblock_date
) VALUES (
    'banned', '$2a$12$aktwRx5l.6h0aoeqzAcBfesdRdwgWmkTdswVca1OMkCcs3TELo/1e',
    '1234', 'Male', 2, '2016-01-01'::timestamp
);
