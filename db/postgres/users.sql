DROP TABLE IF EXISTS users CASCADE;

DROP TYPE IF EXISTS Gender;
CREATE TYPE Gender AS ENUM (
    'Male',
    'Female',
    'Both',
    'Undefined'
);

CREATE TABLE users (
    id            BIGSERIAL PRIMARY KEY,
    username      TEXT UNIQUE NOT NULL,
    pass_hash     TEXT NOT NULL,
    pin           TEXT,
    gender        Gender,
    block_reason  SMALLINT NOT NULL DEFAULT 0,
    unblock_date  TIMESTAMP NOT NULL DEFAULT 'epoch'::timestamp,
    is_gm         BOOL NOT NULL DEFAULT FALSE,
    is_online     BOOL NOT NULL DEFAULT FALSE,
    creation_time TIMESTAMP NOT NULL DEFAULT NOW(),
    login_time    TIMESTAMP NOT NULL DEFAULT 'epoch'::timestamp,
    accepted_eula BOOL NOT NULL DEFAULT FALSE
);
