use std::io::prelude::*;

use std::convert;
use std::io;
use std::result;
use std::string;

use byteorder::{self, LittleEndian, ReadBytesExt, WriteBytesExt};
use encoding::{Encoding, DecoderTrap, EncoderTrap};
use encoding::all::WINDOWS_949;

use io::read_exact;

#[derive(Debug)]
pub enum Error {
    InvalidValue,
    InvalidBoolValue(u8),
    Byteorder(byteorder::Error),
    Io(io::Error),
    FromUtf8(string::FromUtf8Error),
}
pub type Result<T> = result::Result<T, Error>;

impl convert::From<io::Error> for Error {
    fn from (x: io::Error) -> Error {
        Error::Io(x)
    }
}
impl convert::From<string::FromUtf8Error> for Error {
    fn from (x: string::FromUtf8Error) -> Error {
        Error::FromUtf8(x)
    }
}
impl convert::From<byteorder::Error> for Error {
    fn from (x: byteorder::Error) -> Error {
        Error::Byteorder(x)
    }
}

pub trait Writeable {
    fn write (&self, mut w: &mut Write) -> Result<()>;
}
pub trait Readable {
    fn read (mut r: &mut Read) -> Result<Self>;
}

pub fn write<T: Writeable> (mut w: &mut Write, thing: &T) -> Result<()> {
    thing.write(w)
}
pub fn read<T: Readable> (mut r: &mut Read) -> Result<T> {
    Readable::read(r)
}

impl Writeable for String {
    fn write (&self, mut w: &mut Write) -> Result<()> {
        let encoded = &*WINDOWS_949.encode(self, EncoderTrap::Replace).unwrap();
        try!(w.write_u16::<LittleEndian>(encoded.len() as u16));
        try!(w.write_all(encoded));
        Ok(())
    }
}
impl Readable for String {
    fn read (mut r: &mut Read) -> Result<String> {
        let length = try!(r.read_u16::<LittleEndian>());
        Ok(WINDOWS_949.decode(&read_exact(r, length as usize).unwrap(), DecoderTrap::Replace).unwrap())
    }
}

impl Writeable for u8 {
    fn write (&self, mut w: &mut Write) -> Result<()> {
        Ok(try!(w.write_u8(*self)))
    }
}
impl Readable for u8 {
    fn read (mut r: &mut Read) -> Result<u8> {
        Ok(try!(r.read_u8()))
    }
}
impl Writeable for u16 {
    fn write (&self, mut w: &mut Write) -> Result<()> {
        Ok(try!(w.write_u16::<LittleEndian>(*self)))
    }
}
impl Readable for u16 {
    fn read (mut r: &mut Read) -> Result<u16> {
        Ok(try!(r.read_u16::<LittleEndian>()))
    }
}
impl Writeable for u32 {
    fn write (&self, mut w: &mut Write) -> Result<()> {
        Ok(try!(w.write_u32::<LittleEndian>(*self)))
    }
}
impl Readable for u32 {
    fn read (mut r: &mut Read) -> Result<u32> {
        Ok(try!(r.read_u32::<LittleEndian>()))
    }
}
impl Writeable for u64 {
    fn write (&self, mut w: &mut Write) -> Result<()> {
        Ok(try!(w.write_u64::<LittleEndian>(*self)))
    }
}
impl Readable for u64 {
    fn read (mut r: &mut Read) -> Result<u64> {
        Ok(try!(r.read_u64::<LittleEndian>()))
    }
}
impl Writeable for bool {
    fn write (&self, mut w: &mut Write) -> Result<()> {
        Ok(try!(w.write_u8(if *self { 1 } else { 0 })))
    }
}
impl Readable for bool {
    fn read (mut r: &mut Read) -> Result<bool> {
        match try!(r.read_u8()) {
            1   => Ok(true),
            0   => Ok(false),
            x   => Err(Error::InvalidBoolValue(x)),
        }
    }
}
