use prelude::*;

use std::fmt;
use std::ops;
use postgres::types::{ToSql, SessionInfo};

#[derive(Clone)]
pub struct DateTime(pub chrono::DateTime<chrono::UTC>);
impl DateTime {
    pub fn zero_unix() -> DateTime {
        DateTime(chrono::DateTime::<chrono::UTC>::from_utc(
                 chrono::naive::datetime::NaiveDateTime::from_timestamp(0, 0),
                 chrono::UTC)
                )
    }

    //maplestory uses FILETIME
    pub fn zero_filetime() -> DateTime {
        DateTime(chrono::DateTime::<chrono::UTC>::from_utc(
                 chrono::naive::datetime::NaiveDateTime::from_timestamp(-11644473600, 0),
                 chrono::UTC)
                )
    }
}

impl fmt::Debug for DateTime {
    fn fmt (&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", **self)
    }
}

impl postgres::types::FromSql for DateTime {
    fn from_sql<R: Read> (_ty: &postgres::types::Type, raw: &mut R, ctx: &SessionInfo) -> postgres::Result<DateTime> {
        let repr: chrono::DateTime<chrono::UTC> = try!(postgres::types::FromSql::from_sql(&postgres::types::Type::Timestamp, raw, ctx));

        Ok(DateTime(repr))
    }
    fn accepts (ty: &postgres::types::Type) -> bool {
        match *ty {
            postgres::types::Type::Timestamp => true,
            _ => false,
        }
    }
}

impl postgres::types::ToSql for DateTime {
    fn to_sql<W: ?StdSized> (&self, ty: &postgres::types::Type, out: &mut W, ctx: &SessionInfo) -> postgres::Result<postgres::types::IsNull> where W: Write {
        (**self).to_sql(ty, out, ctx)
    }

    fn accepts (ty: &postgres::types::Type) -> bool {
        match *ty {
            postgres::types::Type::Timestamp => true,
            _ => false,
        }
    }

    to_sql_checked!();
}

impl ops::Deref for DateTime {
    type Target = chrono::DateTime<chrono::UTC>;
    fn deref (&self) -> &chrono::DateTime<chrono::UTC> {
        let &DateTime(ref x) = self;
        x
    }
}
impl ops::DerefMut for DateTime {
    fn deref_mut (&mut self) -> &mut chrono::DateTime<chrono::UTC> {
        let &mut DateTime(ref mut x) = self;
        x
    }
}

impl Readable for DateTime {
    fn read(r: &mut Read) -> binary::Result<DateTime> {
        Ok(DateTime(chrono::DateTime::<chrono::UTC>::from_utc(
                    chrono::naive::datetime::NaiveDateTime::from_timestamp(
                        try!(u64::read(r)) as i64 / 10000000 - 11644473600, 0
                    ), chrono::UTC)))
    }
}
impl Writeable for DateTime {
    fn write(&self, mut w: &mut Write) -> binary::Result<()> {
        (self.0.timestamp() as u64).wrapping_add(11644473600).wrapping_mul(10000000).write(&mut w)
    }
}
