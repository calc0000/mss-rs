use prelude::*;

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct AccountId(pub u32);

impl binary::Writeable for AccountId {
    fn write (&self, mut w: &mut Write) -> binary::Result<()> {
        let &AccountId(v) = self;
        binary::write(w, &v)
    }
}
impl binary::Readable for AccountId {
    fn read (mut r: &mut Read) -> binary::Result<AccountId> {
        Ok(AccountId(try!(binary::read(r))))
    }
}
