use prelude::*;

use postgres::error::Error;
use postgres::types::{FromSql, ToSql, Type, IsNull, SessionInfo};

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Gender {
    Male = 0,
    Female = 1,
    Both = 2,
    Undefined = 10,
}

impl Gender {
    pub fn from_u8 (v: u8) -> Option<Gender> {
        match v {
            0 => Some(Gender::Male),
            1 => Some(Gender::Female),
            2 => Some(Gender::Both),
            10 => Some(Gender::Undefined),
            _ => None,
        }
    }
}

impl FromSql for Gender {
    fn accepts (ty: &Type) -> bool {
        match ty {
            &Type::Other(ref other) => other.name() == "gender",
            _ => false,
        }
    }

    fn from_sql<R: Read> (ty: &Type, raw: &mut R, ctx: &SessionInfo) -> postgres::Result<Gender> {
        if !<Gender as FromSql>::accepts(ty) {
            return Err(Error::WrongType(ty.clone()));
        }

        let value: String = try!(FromSql::from_sql(&Type::Text, raw, ctx));
        match &value[..] {
            "Male" => Ok(Gender::Male),
            "Female" => Ok(Gender::Female),
            "Both" => Ok(Gender::Both),
            "Undefined" => Ok(Gender::Undefined),
            _ => unreachable!(),
        }
    }
}

impl ToSql for Gender {
    to_sql_checked!();

    fn accepts (ty: &Type) -> bool {
        match ty {
            &Type::Other(ref other) => other.name() == "gender",
            _ => false,
        }
    }

    fn to_sql<W: ?StdSized> (&self, ty: &Type, out: &mut W, ctx: &SessionInfo) -> postgres::Result<IsNull> where W: Write {
        if !<Gender as FromSql>::accepts(ty) {
            return Err(Error::WrongType(ty.clone()));
        }

        ToSql::to_sql(&match *self {
            Gender::Male => "Male",
            Gender::Female => "Female",
            Gender::Both => "Both",
            Gender::Undefined => "Undefined",
        }, &Type::Text, out, ctx)
    }
}

impl Readable for Gender {
    fn read(r: &mut Read) -> binary::Result<Gender> {
        Ok(Gender::from_u8(try!(Readable::read(r))).unwrap())
    }
}
impl Writeable for Gender {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        let gender: u8 = match *self {
            Gender::Male => 0,
            Gender::Female => 1,
            Gender::Both => 2,
            Gender::Undefined => 10
        };
        binary::write(w, &gender)
    }
}
